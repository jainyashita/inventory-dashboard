## Multi-Inventory-cum-Ecommerce Management System

> A comprehensive solution designed to manage multiple inventories and streamline ecommerce operations.

![preview](public/assets/preview.png)

## Refer to the docs for detailed description of the product with real time video reference.

[Multi-Inventory-cum-Ecommerce Management System Documentation](https://drive.google.com/file/d/1ix2KBLFybTOp7eseX_eCBLZiLTv41oIt/view?usp=sharing)

## Quick start

- Clone the repo : `git clone https://gitlab.com/jainyashita/inventory-dashboard.git`
- Recommended `Node.js v18.x`.
- **Install:** `yarn install`
- **Start:** `yarn dev`
- **Build:** `yarn build`
