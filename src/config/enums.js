// enums.js

export const TransactionStatus = {
  PAID: 'PAID',
  UNPAID: 'UNPAID',
};

export const OrderStatus = {
  COMPLETED: 'COMPLETED',
  SHIPPED: 'SHIPPED',
  PACKING: 'PACKING',
  // Add more statuses as needed
};

export const PaymentMode = {
  CASH: 'CASH',
  UPI: 'UPI',
  CARD: 'CARD',
  CREDIT: 'CREDIT',
  // Add more payment modes as needed
};

export const UserType = {
  ADMIN: 'ADMIN',
  INVENTORY_MANAGER: 'INVENTORY_MANAGER',
  WORKER: 'WORKER',
};
