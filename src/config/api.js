// src/config/api.js

const API_BASE_URL = 'http://localhost:5000';

export const endpoints = {
  LOGIN: '/api/login',
  REGISTER: '/api/register',
  USER: '/api',
  WORKER: '/workers',
  INVENTORYLIST: '/inventory',
  CATEGORYLIST: '/category',
  ADDRESS: '/address',
  GSTSLAB: '/gstSlab',
  PRODUCT: '/product',
  ORDER: '/order',
  COUNT: '/count',
  STATS: '/stats',
  PRODUCTCOUNT: '/product-count',
  CATEGORYPRODUCTCOUNT: '/category-product-count',
  UPLOADIMAGE: '/uploadImage',
  REMOVEIMAGE: '/removeImage',
  STOCK: '/stock',
  PRODUCTBELOWROP: '/products-below-rop',
  // Add other endpoints here
};

export default API_BASE_URL;
