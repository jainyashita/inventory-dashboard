import React from 'react';
import PropTypes from 'prop-types';

import { Alert, Portal, Snackbar } from '@mui/material';

const CustomSnackbar = ({ open, message, onClose, severity }) => (
  <Portal>
    <div style={{ position: 'fixed', bottom: 0, right: 0, width: '100%', height: 'auto' }}>
      <Snackbar
        open={open}
        autoHideDuration={6000}
        onClose={onClose}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
        transformOrigin={{ vertical: 'bottom', horizontal: 'right' }}
      >
        <Alert onClose={onClose} severity={severity}>
          {message}
        </Alert>
      </Snackbar>
    </div>
  </Portal>
);

export default CustomSnackbar;

CustomSnackbar.propTypes = {
  open: PropTypes.any,
  onClose: PropTypes.func,
  message: PropTypes.any,
  severity: PropTypes.any,
};
