import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';

import { Add as AddIcon, Delete as DeleteIcon } from '@mui/icons-material';
import {
  Box,
  Alert,
  Button,
  Select,
  Dialog,
  MenuItem,
  TextField,
  InputLabel,
  Typography,
  IconButton,
  FormControl,
  DialogTitle,
  DialogContent,
  DialogActions,
} from '@mui/material';

export default function GeneralEditModal({
  open,
  handleClose,
  data,
  schema,
  dropdownOptions,
  handleSave,
  messageHeading,
}) {
  const initialFormData = Object.fromEntries(
    schema.map((field) => [
      field.name,
      field.type === 'object' ? {} : '', // Initialize objects as empty objects, others as empty strings
    ])
  );

  const [formData, setFormData] = useState(initialFormData);
  const [newSpecKey, setNewSpecKey] = useState('');
  const [newSpecValue, setNewSpecValue] = useState('');
  const [validationErrors, setValidationErrors] = useState({});
  const [showAlert, setShowAlert] = useState(false);

  useEffect(() => {
    if (data) {
      setFormData(data);
    } else {
      setFormData(initialFormData); // Reset to initial empty state
    }
  }, [data]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    const nameParts = name.split('.');

    if (nameParts.length === 1) {
      setFormData((prevData) => ({
        ...prevData,
        [name]: value,
      }));
    } else {
      setFormData((prevData) => {
        const updatedData = { ...prevData };
        let nestedObject = updatedData;

        for (let i = 0; i < nameParts.length - 1; i += 1) {
          nestedObject = nestedObject[nameParts[i]];
        }

        nestedObject[nameParts[nameParts.length - 1]] = value;
        return updatedData;
      });
    }

    if (validationErrors[name]) {
      setValidationErrors((prevErrors) => {
        const newErrors = { ...prevErrors };
        delete newErrors[name];
        return newErrors;
      });
    }
  };

  const handleSubmit = () => {
    const newErrors = {};

    schema.forEach((field) => {
      if (field.required && !formData[field.name]) {
        newErrors[field.name] = true;
      }
    });

    if (Object.keys(newErrors).length > 0) {
      setValidationErrors(newErrors);
      setShowAlert(true);
    } else {
      setShowAlert(false);
      handleSave(formData);
    }
  };

  const addNewSpec = () => {
    if (newSpecKey && newSpecValue) {
      setFormData((prevData) => ({
        ...prevData,
        specifications: {
          ...prevData.specifications,
          [newSpecKey]: newSpecValue,
        },
      }));
      setNewSpecKey('');
      setNewSpecValue('');
    }
  };

  const deleteSpec = (key) => {
    if (formData.specifications) {
      const updatedSpecs = { ...formData.specifications };
      delete updatedSpecs[key];
      setFormData((prevData) => ({
        ...prevData,
        specifications: updatedSpecs,
      }));
    }
  };

  const renderField = (field, prefix = '') => {
    const fieldName = prefix ? `${prefix}.${field.name}` : field.name;

    if (field.type === 'object' && typeof formData[field.name] === 'object') {
      return (
        <Box key={field.name} mb={2}>
          <Typography variant="h6">{field.label}</Typography>
          {Object.keys(formData[field.name]).map((key) => (
            <Box key={key} display="flex" alignItems="center">
              <TextField
                margin="dense"
                name={`${fieldName}.${key}`}
                label={key.charAt(0).toUpperCase() + key.slice(1)}
                type="text"
                value={formData[field.name][key]}
                onChange={handleChange}
                fullWidth
                error={!!validationErrors[`${fieldName}.${key}`]}
              />
              <IconButton color="secondary" onClick={() => deleteSpec(key)}>
                <DeleteIcon />
              </IconButton>
            </Box>
          ))}
          <Box display="flex" alignItems="center">
            <TextField
              margin="dense"
              name="newSpecKey"
              label="New Specification Key"
              type="text"
              value={newSpecKey}
              onChange={(e) => setNewSpecKey(e.target.value)}
            />
            <TextField
              margin="dense"
              name="newSpecValue"
              label="New Specification Value"
              type="text"
              value={newSpecValue}
              onChange={(e) => setNewSpecValue(e.target.value)}
            />
            <IconButton color="primary" onClick={addNewSpec}>
              <AddIcon />
            </IconButton>
          </Box>
        </Box>
      );
    }
    if (field.type === 'dropdown') {
      return (
        <FormControl
          fullWidth
          margin="normal"
          key={fieldName}
          error={!!validationErrors[fieldName]}
        >
          <InputLabel>{field.label}</InputLabel>
          <Select
            name={fieldName}
            value={formData[field.name] || ''}
            onChange={handleChange}
            label={field.label}
            required={field.required}
          >
            {dropdownOptions[field.name]?.map((option, index) => (
              <MenuItem key={index} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      );
    }
    return (
      <TextField
        key={fieldName}
        margin="dense"
        name={fieldName}
        label={field.label}
        type={field.type || 'text'}
        fullWidth
        value={formData[field.name] || ''}
        onChange={handleChange}
        disabled={field.disabled}
        required={field.required}
        error={!!validationErrors[fieldName]}
      />
    );
  };

  return (
    <Dialog open={open} onClose={handleClose}>
      <DialogTitle>{messageHeading}</DialogTitle>
      <DialogContent>
        {schema.map((field) => renderField(field))}
        {showAlert && (
          <Alert severity="error" sx={{ mt: 2 }}>
            Please fill out all required fields.
          </Alert>
        )}
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Cancel</Button>
        <Button onClick={handleSubmit} color="primary">
          Save
        </Button>
      </DialogActions>
    </Dialog>
  );
}

GeneralEditModal.propTypes = {
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  messageHeading: PropTypes.string,
  data: PropTypes.object, // data prop can be undefined when creating new product
  schema: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
      type: PropTypes.string,
      disabled: PropTypes.bool,
      autoFocus: PropTypes.bool,
      required: PropTypes.bool,
    })
  ).isRequired,
  dropdownOptions: PropTypes.object, // An object mapping field names to arrays of option objects
  handleSave: PropTypes.func.isRequired,
};
