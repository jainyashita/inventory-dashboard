import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';

import {
  Button,
  Dialog,
  Select,
  MenuItem,
  TextField,
  InputLabel,
  FormControl,
  DialogTitle,
  DialogContent,
  DialogActions,
} from '@mui/material';

export default function GeneralEditModal({
  open,
  handleClose,
  data,
  schema,
  dropdownOptions,
  handleSave,
  messageHeading,
}) {
  const initialFormData = Object.fromEntries(
    schema.map((field) => [
      field.name,
      field.type === 'object' ? {} : '', // Initialize objects as empty objects, others as empty strings
    ])
  );

  const [formData, setFormData] = useState(initialFormData);

  useEffect(() => {
    if (data) {
      setFormData(data);
    } else {
      setFormData(initialFormData); // Reset to initial empty state
    }
  }, [data]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    const nameParts = name.split('.');

    if (nameParts.length === 1) {
      setFormData((prevData) => ({
        ...prevData,
        [name]: value,
      }));
    } else {
      setFormData((prevData) => {
        const updatedData = { ...prevData };
        let nestedObject = updatedData;

        for (let i = 0; i < nameParts.length - 1; i += 1) {
          nestedObject = nestedObject[nameParts[i]];
        }

        nestedObject[nameParts[nameParts.length - 1]] = value;
        return updatedData;
      });
    }
  };

  const handleSubmit = () => {
    handleSave(formData);
  };

  const renderField = (field, prefix = '') => {
    const fieldName = prefix ? `${prefix}.${field.name}` : field.name;

    if (field.type === 'dropdown') {
      return (
        <FormControl fullWidth margin="normal" key={fieldName}>
          <InputLabel>{field.label}</InputLabel>
          <Select
            name={fieldName}
            value={formData[field.name] || ''}
            onChange={handleChange}
            label={field.label}
          >
            {dropdownOptions[field.name]?.map((option, index) => (
              <MenuItem key={index} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      );
    }

    return (
      <TextField
        key={fieldName}
        margin="dense"
        name={fieldName}
        label={field.label}
        type={field.type || 'text'}
        fullWidth
        value={formData[field.name] || ''}
        onChange={handleChange}
        disabled={field.disabled}
      />
    );
  };

  return (
    <Dialog open={open} onClose={handleClose}>
      <DialogTitle>{messageHeading}</DialogTitle>
      <DialogContent>{schema.map((field) => renderField(field))}</DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Cancel</Button>
        <Button onClick={handleSubmit} color="primary">
          Save
        </Button>
      </DialogActions>
    </Dialog>
  );
}

GeneralEditModal.propTypes = {
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  messageHeading: PropTypes.string,
  data: PropTypes.object, // data prop can be undefined when creating new product
  schema: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
      type: PropTypes.oneOf(['text', 'number', 'dropdown']),
      disabled: PropTypes.bool,
      autoFocus: PropTypes.bool,
    })
  ).isRequired,
  dropdownOptions: PropTypes.object, // An object mapping field names to arrays of option objects
  handleSave: PropTypes.func.isRequired,
};
