import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import {
  Box,
  List,
  Dialog,
  Button,
  ListItem,
  TextField,
  IconButton,
  Typography,
  DialogTitle,
  ListItemText,
  DialogContent,
  DialogActions,
} from '@mui/material';
import {
  ArrowForward as ArrowForwardIcon,
  AddCircleOutlineRounded as AddCircleOutlineRoundedIcon,
  RemoveCircleOutlineRounded as RemoveCircleOutlineRoundedIcon,
} from '@mui/icons-material/';

import { useRouter } from 'src/routes/hooks';

import { NewAddress } from 'src/redux/api/user';
import { NewOrder } from 'src/redux/api/orders';
import { TransactionStatus } from 'src/config/enums';
import { addressSchema } from 'src/redux/schema/db-schemas';
import { clearCart, updateQuantity } from 'src/redux/reducers/cartSlice';

import OrderInvoice from 'src/sections/order/order-invoice';

const CartModal = ({ open, handleClose }) => {
  const history = useRouter();
  const dispatch = useDispatch();
  const today = new Date();
  const [error, setError] = useState(null);
  const [showCart, setShowCart] = useState(true);
  const [showMessage, setShowMessage] = useState(false);
  const [showAddress, setShowAddress] = useState(false);
  const [showInvoice, setShowInvoice] = useState(false);
  const [customerInfo, setCustomerInfo] = useState({ name: '', mobile: '' });
  const [orderData, setOrderData] = useState(null);
  const [addressData, setAddressData] = useState({});
  const month = today.getMonth() + 1;
  const year = today.getFullYear();
  const date = today.getDate();
  const currentDate = `${year}-${month}-${date}`;

  const items = useSelector((state) => state.cart.items);
  const { token, user, inventory } = useSelector((state) => state.auth);
  const totalAmount = useSelector((state) => state.cart.totalAmount);

  const handleClick = () => {
    history.push(`/category/`);
  };

  const handleAddressForm = () => {
    setShowCart(false);
    setShowAddress(true);
  };

  const handleCartClose = () => {
    setShowCart(true);
    setShowAddress(false);
    setShowMessage(false);
    setShowInvoice(false);
    handleClose();
  };
  const handleCustomerInfoChange = (e) => {
    const { name, value } = e.target;
    setCustomerInfo((prevInfo) => ({
      ...prevInfo,
      [name]: value,
    }));
  };

  const handleInputChange = (e, name) => {
    setAddressData((prevData) => ({
      ...prevData,
      [name]: e.target.value,
    }));
  };

  console.log(items);
  const handleNewOrder = async () => {
    try {
      const data = {
        addressLine1: addressData.addressLine1,
        addressLine2: addressData.addressLine2,
        landmark: addressData.landmark,
        city: addressData.city,
        country: addressData.country,
        postalCode: addressData.postalCode,
      };
      console.log(data);
      const addressResponse = await NewAddress({
        token,
        data,
      });
      console.log(addressResponse);
      const itemsWithGst = await Promise.all(
        items.map(async (item) => {
          const rate = item.gstRate / 100;
          console.log(rate, 'rate');
          const gstAmount = item.price * rate * item.quantity;
          return {
            ...item,
            gstAmount,
            totalPriceWithGst: item.totalPrice + gstAmount,
          };
        })
      );

      const totalAmountWithGst = itemsWithGst.reduce(
        (acc, item) => acc + item.totalPriceWithGst,
        0
      );

      const orderDetails = {
        user: user._id,
        orderDate: currentDate,
        orderStatus: 'PACKING',
        address: addressResponse._id,
        inventory,
        totalAmount,
        products: itemsWithGst,
        transactionStatus: TransactionStatus.UNPAID,
        customerName: customerInfo.name,
        customerMobile: customerInfo.mobile,
        totalAmountWithGst,
      };
      console.log('order details', orderDetails);
      const response = await NewOrder({
        token,
        data: orderDetails,
      });
      setOrderData(response); // Save order data to state
      setShowAddress(false);
      setShowMessage(false);
      setShowInvoice(true);
      dispatch(clearCart());
    } catch (err) {
      setError(err);
    }
  };

  if (error) {
    return <div>Error: {error.message}</div>;
  }
  return (
    <Dialog open={open} onClose={handleClose}>
      {showCart && (
        <>
          <DialogTitle sx={{ my: 1 }}>Cart</DialogTitle>
          <DialogContent sx={{ p: 2, pb: 0, minHeight: '300px', minWidth: '600px' }}>
            <List>
              {items.map((item) => (
                <ListItem key={item.id} sx={{ py: 1, mt: 0.5, backgroundColor: '#f5f5f5' }}>
                  <ListItemText
                    primary={item.name}
                    secondary={`₹${item.totalPrice} (x${item.quantity})`}
                    sx={{ mr: 4 }}
                  />
                  <IconButton onClick={() => dispatch(updateQuantity({ id: item.id, change: -1 }))}>
                    <RemoveCircleOutlineRoundedIcon />
                  </IconButton>
                  <span>{item.quantity}</span>
                  <IconButton onClick={() => dispatch(updateQuantity({ id: item.id, change: 1 }))}>
                    <AddCircleOutlineRoundedIcon />
                  </IconButton>
                </ListItem>
              ))}
            </List>

            {totalAmount > 0 ? (
              <Typography variant="h6" sx={{ my: 2, textAlign: 'right' }}>
                Total Amount: ₹{totalAmount}
              </Typography>
            ) : (
              <div style={{ textAlign: 'center', marginTop: 20 }}>
                <Typography>Your cart is empty</Typography>
                <Button variant="outlined" sx={{ mt: 4 }} onClick={handleClick}>
                  Explore More
                </Button>
              </div>
            )}
          </DialogContent>
          <DialogActions sx={{ px: 2, py: 1 }}>
            <Button variant="contained" color="primary" onClick={() => dispatch(clearCart())}>
              Clear Cart
            </Button>
            <Button variant="contained" color="primary" onClick={handleAddressForm}>
              Proceed
              <ArrowForwardIcon />
            </Button>
            <Button onClick={handleCartClose} color="primary">
              Close
            </Button>
          </DialogActions>
        </>
      )}

      {showAddress && (
        <>
          <DialogTitle sx={{ my: 1 }}>Enter Customer Details</DialogTitle>
          <DialogContent sx={{ p: 2, pb: 0, minWidth: '600px', mb: 2 }}>
            {' '}
            <Box sx={{ mb: 2 }}>
              <TextField
                fullWidth
                label="Name"
                name="name"
                value={customerInfo.name}
                onChange={handleCustomerInfoChange}
                sx={{ mb: 2 }}
              />
              <TextField
                fullWidth
                label="Mobile"
                name="mobile"
                value={customerInfo.mobile}
                onChange={handleCustomerInfoChange}
              />
            </Box>
          </DialogContent>
          <DialogTitle sx={{ my: 1 }}>Enter Shipping Address</DialogTitle>
          <DialogContent sx={{ p: 2, pb: 0, minHeight: '300px', minWidth: '600px' }}>
            {/* <FormBuilder schema={addressSchema} onSubmit={handleAddressSave} /> */}
            {addressSchema.map((field) => (
              <TextField
                key={field.name}
                fullWidth
                label={field.label}
                name={field.name}
                value={addressData[field.name] || ''}
                onChange={(e) => handleInputChange(e, field.name)}
                sx={{ mb: 2 }}
              />
            ))}{' '}
            {showMessage && (
              <Typography color="secondary" sx={{ my: 2, textAlign: 'right' }}>
                Address saved successfully
              </Typography>
            )}
          </DialogContent>
          <DialogActions sx={{ px: 2, py: 1 }}>
            <Button variant="contained" color="primary" onClick={handleNewOrder}>
              Create Order Invoice
            </Button>
            <Button onClick={handleCartClose} color="primary">
              Close
            </Button>
          </DialogActions>
        </>
      )}

      {showInvoice && orderData && (
        <>
          <OrderInvoice currentDate={currentDate} orderData={orderData} token={token} />
          <DialogActions sx={{ px: 2, py: 1 }}>
            <Button onClick={handleCartClose} color="primary">
              Close
            </Button>
          </DialogActions>
        </>
      )}
    </Dialog>
  );
};

export default CartModal;

CartModal.propTypes = {
  handleClose: PropTypes.any,
  open: PropTypes.any,
};
