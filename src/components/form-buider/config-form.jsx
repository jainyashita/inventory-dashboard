const formSchema = [
  { name: 'name', label: 'Name', type: 'text', defaultValue: 'John Doe' },
  { name: 'email', label: 'Email', type: 'email', disabled: true },
  { name: 'gender', label: 'Gender', type: 'select', options: ['Male', 'Female', 'Other'] },
  { name: 'subscribe', label: 'Subscribe', type: 'checkbox', defaultValue: true, disabled: true },
];
export default formSchema;

// const handleSubmit = (formData) => {
//   console.log(formData); // You can handle the form data here
// };
// <FormBuilder schema={formSchema} onSubmit={handleSubmit} />
