import PropTypes from 'prop-types';

import {
  Button,
  Select,
  MenuItem,
  Checkbox,
  TextField,
  InputLabel,
  FormControl,
  FormControlLabel,
} from '@mui/material';

const fieldComponents = {
  text: ({ name, label, defaultValue, disabled }) => (
    <TextField
      name={name}
      label={label}
      defaultValue={defaultValue}
      disabled={disabled}
      fullWidth
    />
  ),
  email: ({ name, label, defaultValue, disabled }) => (
    <TextField
      name={name}
      label={label}
      type="email"
      defaultValue={defaultValue}
      disabled={disabled}
      fullWidth
    />
  ),
  select: ({ name, label, options, defaultValue, disabled }) => (
    <div>
      <FormControl fullWidth>
        {label && <InputLabel id={`${name}-label`}>{label}</InputLabel>}
        <Select
          label={label}
          labelId={`${name}-label`}
          id={name}
          name={name}
          defaultValue={defaultValue}
          disabled={disabled}
          fullWidth
        >
          {options.map((option) => (
            <MenuItem key={option} value={option}>
              {option}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  ),
  checkbox: ({ name, label, defaultValue, disabled }) => (
    <FormControlLabel
      control={<Checkbox name={name} defaultChecked={defaultValue} disabled={disabled} />}
      label={label}
    />
  ),
};

// Form Builder Component
export default function FormBuilder({ schema, onSubmit }) {
  const handleSubmit = (event) => {
    event.preventDefault();
    const formData = new FormData(event.target);
    const formValues = Object.fromEntries(formData.entries());
    onSubmit(formValues);
  };

  return (
    <form onSubmit={handleSubmit}>
      {schema.map((field) => {
        const FieldComponent = fieldComponents[field.type];
        const { defaultValue = '', disabled = false } = field;
        return (
          <div key={field.name} style={{ marginBottom: '1rem' }}>
            <FieldComponent {...field} defaultValue={defaultValue} disabled={disabled} />
          </div>
        );
      })}
      <Button type="submit" variant="outlined" color="secondary">
        Save Address
      </Button>
    </form>
  );
}
// PropTypes validation
FormBuilder.propTypes = {
  schema: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
      type: PropTypes.oneOf(['text', 'email', 'select', 'checkbox']).isRequired,
      options: PropTypes.arrayOf(PropTypes.string), // Only required for select type
    })
  ).isRequired,
  onSubmit: PropTypes.func.isRequired,
};
