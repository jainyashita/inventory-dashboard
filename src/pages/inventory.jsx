import { Helmet } from 'react-helmet-async';

import { InventoryView } from 'src/sections/inventory/view';

// ----------------------------------------------------------------------

export default function InventoryPage() {
  console.log('inventory');
  return (
    <>
      <Helmet>
        <title> Inventory </title>
      </Helmet>

      <InventoryView />
    </>
  );
}
