import { Helmet } from 'react-helmet-async';

import { GstSlabView } from 'src/sections/gst-slab/view';

// ----------------------------------------------------------------------

export default function GstSlabPage() {
  return (
    <>
      <Helmet>
        <title> GST Slab</title>
      </Helmet>

      <GstSlabView />
    </>
  );
}
