import { lazy, Suspense } from 'react';
import { useSelector } from 'react-redux';
import { Outlet, Navigate, useRoutes, useNavigate, useLocation } from 'react-router-dom';

import { history } from 'src/utils/history';

import DashboardLayout from 'src/layouts/dashboard';
import AuthMiddleware from 'src/features/auth/authRoute';

import ProductDetail from 'src/sections/products/product-detail';

export const IndexPage = lazy(() => import('src/pages/app'));
export const BlogPage = lazy(() => import('src/pages/blog'));
export const UserPage = lazy(() => import('src/pages/user'));
export const LoginPage = lazy(() => import('src/pages/login'));
export const ProductsPage = lazy(() => import('src/pages/products'));
export const Page404 = lazy(() => import('src/pages/page-not-found'));
export const InventoryPage = lazy(() => import('src/pages/inventory'));
export const CategoryPage = lazy(() => import('src/pages/category'));
export const GstSlabPage = lazy(() => import('src/pages/gst-slab'));
export const OrdersPage = lazy(() => import('src/pages/order'));

// ----------------------------------------------------------------------

export default function Router() {
  const { user } = useSelector((state) => state.auth);

  const routes = useRoutes([
    {
      element: (
        <DashboardLayout>
          <Suspense>
            <Outlet />
          </Suspense>
        </DashboardLayout>
      ),
      children: [
        {
          element: (
            <AuthMiddleware>
              <IndexPage />
            </AuthMiddleware>
          ),
          index: true,
        },
        // {
        //   path: 'user',
        //   element: (
        //     <AuthMiddleware>
        //       <UserPage />
        //     </AuthMiddleware>
        //   ),
        // },
        ...(user?.userType === 'ADMIN' || user?.userType === 'INVENTORY_MANAGER'
          ? [
              {
                path: 'user',
                element: (
                  <AuthMiddleware>
                    <UserPage />
                  </AuthMiddleware>
                ),
              },
            ]
          : []),
        {
          path: 'category/:id/products',
          element: (
            <AuthMiddleware>
              {' '}
              <ProductsPage />{' '}
            </AuthMiddleware>
          ),
        },
        {
          path: '/product/:id',
          element: (
            <AuthMiddleware>
              {' '}
              <ProductDetail />{' '}
            </AuthMiddleware>
          ),
        },
        {
          path: '/order/',
          element: (
            <AuthMiddleware>
              {' '}
              <OrdersPage />{' '}
            </AuthMiddleware>
          ),
        },
        {
          path: 'blog',
          element: (
            <AuthMiddleware>
              <BlogPage />
            </AuthMiddleware>
          ),
        },
        {
          path: 'inventory',
          element: (
            <AuthMiddleware>
              <InventoryPage />
            </AuthMiddleware>
          ),
        },
        {
          path: 'category',
          element: (
            <AuthMiddleware>
              <CategoryPage />
            </AuthMiddleware>
          ),
        },
        {
          path: 'gstSlab',
          element: (
            <AuthMiddleware>
              <GstSlabPage />
            </AuthMiddleware>
          ),
        },
      ],
    },
    {
      path: 'login',
      element: <LoginPage />,
    },
    {
      path: '404',
      element: <Page404 />,
    },
    {
      path: '*',
      element: <Navigate to="/404" replace />,
    },
  ]);

  // init custom history object to allow navigation from
  // anywhere in the react app (inside or outside components)
  history.navigate = useNavigate();
  history.location = useLocation();
  console.log(history);
  return routes;
}
