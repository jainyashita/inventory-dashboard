// authMiddleware.js
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { Navigate } from 'react-router-dom';

import { history } from 'src/utils/history';

const AuthMiddleware = ({ children }) => {
  const { token, inventory } = useSelector((state) => state.auth);
  console.log('here', token, inventory);
  // Check if the user is authenticated
  const isAuthenticated = !!token;

  // If the user is authenticated, render the children (routes)
  // Otherwise, redirect to the login page
  return isAuthenticated ? (
    children
  ) : (
    <Navigate to="/login" replace state={{ from: history.location }} />
  );
};

AuthMiddleware.propTypes = {
  children: PropTypes.node.isRequired,
};

export default AuthMiddleware;
