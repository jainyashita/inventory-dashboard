import { useState } from 'react';
import PropTypes from 'prop-types';

import Stack from '@mui/material/Stack';
import Popover from '@mui/material/Popover';
import TableRow from '@mui/material/TableRow';
import Checkbox from '@mui/material/Checkbox';
import MenuItem from '@mui/material/MenuItem';
import TableCell from '@mui/material/TableCell';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import { Button, Dialog, DialogTitle, DialogActions, DialogContent } from '@mui/material';

import { gstSlabSchema } from 'src/redux/schema/db-schemas';
import { DeleteGstSlab, UpdateGstSlab } from 'src/redux/api/gst-slab';

import Iconify from 'src/components/iconify';
import GeneralEditModal from 'src/components/form-modal';
import CustomSnackbar from 'src/components/custom-snackbar';

// ----------------------------------------------------------------------

export default function GstSlabTableRow({
  selected,
  name,
  rate,
  id,
  handleClick,
  token,
  fetchData,
}) {
  const [open, setOpen] = useState(null);
  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  const [isDeleteDialogOpen, setIsDeleteDialogOpen] = useState(false);
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');
  const [snackbarSeverity, setSnackbarSeverity] = useState('success');

  const handleOpenMenu = (event) => {
    setOpen(event.currentTarget);
  };

  const handleCloseMenu = () => {
    setOpen(null);
  };

  const handleOpenEditModal = () => {
    setIsEditModalOpen(true);
    handleCloseMenu();
  };

  const handleCloseEditModal = () => {
    setIsEditModalOpen(false);
  };

  const handleOpenDeleteDialog = () => {
    setIsDeleteDialogOpen(true);
    handleCloseMenu();
  };

  const handleCloseDeleteDialog = () => {
    setIsDeleteDialogOpen(false);
  };
  const handleSnackbarClose = () => {
    setSnackbarOpen(false);
  };

  const handleEditSave = async (formData) => {
    console.log(token, id);
    try {
      const response = await UpdateGstSlab({ token, id, formData });
      console.log(response);

      // Close the modal after saving
      setSnackbarMessage('Update successful');
      setSnackbarSeverity('success');
      fetchData();
    } catch (error) {
      console.error('Error updating gst slab:', error);
      setSnackbarMessage('Error updating gst slab');
      setSnackbarSeverity('error');
    }
    setSnackbarOpen(true);
    setIsEditModalOpen(false);
  };

  const handleDeleteSave = async (formData) => {
    console.log(token, id);
    try {
      const response = await DeleteGstSlab({ token, id });
      console.log(response);
      setSnackbarMessage('Gst Slab deleted successfully');
      setSnackbarSeverity('success');
      fetchData();
    } catch (error) {
      console.error('Error deleting gst slab:', error);
      setSnackbarMessage('Error deleting gst slab');
      setSnackbarSeverity('error');
    }
    setSnackbarOpen(true);
    handleCloseDeleteDialog(false);
  };

  return (
    <>
      <TableRow hover tabIndex={-1} role="checkbox" selected={selected}>
        <TableCell padding="checkbox">
          <Checkbox disableRipple checked={selected} onChange={handleClick} />
        </TableCell>

        <TableCell component="th" scope="row" padding="none">
          <Stack direction="row" alignItems="center" spacing={2}>
            <Typography variant="subtitle2" noWrap>
              {name}
            </Typography>
          </Stack>
        </TableCell>

        <TableCell>{rate}</TableCell>
        <TableCell align="right">
          <IconButton onClick={handleOpenMenu}>
            <Iconify icon="eva:more-vertical-fill" />
          </IconButton>
        </TableCell>
      </TableRow>

      <Popover
        open={!!open}
        anchorEl={open}
        onClose={handleCloseMenu}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        PaperProps={{
          sx: { width: 140 },
        }}
      >
        <MenuItem onClick={handleOpenEditModal}>
          <Iconify icon="eva:edit-fill" sx={{ mr: 2 }} />
          Edit
        </MenuItem>

        <MenuItem onClick={handleOpenDeleteDialog} sx={{ color: 'error.main' }}>
          <Iconify icon="eva:trash-2-outline" sx={{ mr: 2 }} />
          Delete
        </MenuItem>
      </Popover>

      <GeneralEditModal
        messageHeading="Edit Item"
        open={isEditModalOpen}
        handleClose={handleCloseEditModal}
        data={{
          id,
          name,
          rate,
        }}
        schema={gstSlabSchema}
        handleSave={handleEditSave}
      />

      <CustomSnackbar
        open={snackbarOpen}
        onClose={handleSnackbarClose}
        message={snackbarMessage}
        severity={snackbarSeverity}
      />
      <Dialog open={isDeleteDialogOpen} onClose={handleCloseDeleteDialog}>
        <DialogTitle>Confirm Delete</DialogTitle>
        <DialogContent>
          <Typography>Are you sure you want to delete this item?</Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseDeleteDialog} color="primary">
            No
          </Button>
          <Button onClick={handleDeleteSave} color="secondary">
            Yes
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

GstSlabTableRow.propTypes = {
  name: PropTypes.any,
  rate: PropTypes.any,
  id: PropTypes.any,
  token: PropTypes.any,
  handleClick: PropTypes.any,
  fetchData: PropTypes.any,
  selected: PropTypes.any,
};
