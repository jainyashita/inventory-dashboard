import { useSelector } from 'react-redux';
import { useState, useEffect } from 'react';

import Container from '@mui/material/Container';
import {
  Card,
  Stack,
  Table,
  Button,
  TableBody,
  Typography,
  TableContainer,
  TablePagination,
} from '@mui/material';

import { UserList, NewAddress } from 'src/redux/api/user';
import { inventorySchema } from 'src/redux/schema/db-schemas';
import { NewInventory, InventoryList } from 'src/redux/api/inventory';

import Iconify from 'src/components/iconify';
import Scrollbar from 'src/components/scrollbar';
import GeneralEditModal from 'src/components/form-modal';
import CustomSnackbar from 'src/components/custom-snackbar';

import TableNoData from 'src/sections/user/table-no-data';
import UserTableHead from 'src/sections/user/user-table-head';
import TableEmptyRows from 'src/sections/user/table-empty-rows';
import UserTableToolbar from 'src/sections/user/user-table-toolbar';
import { emptyRows, applyFilter, getComparator } from 'src/sections/user/utils';

import InventoryTableRow from '../inventory-table-row';

export default function InventoryPage() {
  const [inventory, setInventory] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const { token } = useSelector((state) => state.auth);
  const [selected, setSelected] = useState([]);
  const [filterName, setFilterName] = useState('');
  const [page, setPage] = useState(0);
  const [order, setOrder] = useState('asc');
  const [orderBy, setOrderBy] = useState('name');
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [isInventoryModalOpen, setIsInventoryModalOpen] = useState(false);
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');
  const [snackbarSeverity, setSnackbarSeverity] = useState('success');
  const [dropdownOptions, setDropdownOptions] = useState({});

  const fetchData = async () => {
    try {
      const data = await InventoryList(token);
      console.log(data);

      setInventory(data);
      setLoading(false);
    } catch (err) {
      setError(err);
      setLoading(false);
    }
  };
  useEffect(() => {
    const fetchUsers = async () => {
      try {
        const users = await UserList(token);
        setDropdownOptions((prevState) => ({
          ...prevState,
          owner: users.map((user) => ({
            label: `${user.FirstName} ${user.LastName}`,
            value: user._id,
          })),
        }));
        console.log(dropdownOptions);
      } catch (err) {
        console.error('Failed to fetch owner', err);
      }
    };
    fetchUsers();
    fetchData();
  }, [token]);

  const handleSort = (event, id) => {
    const isAsc = orderBy === id && order === 'asc';
    if (id !== '') {
      setOrder(isAsc ? 'desc' : 'asc');
      setOrderBy(id);
    }
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = inventory.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];
    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleNewInventory = async (data) => {
    try {
      const addressData = {
        addressLine1: data.addressLine1,
        addressLine2: data.addressLine2,
        landmark: data.landmark,
        city: data.city,
        country: data.country,
        postalCode: data.postalCode,
      };
      const addressResponse = await NewAddress({
        token,
        data: addressData,
      });
      const inventoryData = {
        name: data.name,
        owner: data.owner,
        location: addressResponse._id,
        inventoryManager: data.inventoryManager,
      };
      const response = await NewInventory({ token, data: inventoryData });
      console.log(response);
      fetchData();
      setSnackbarMessage('Update successful');
      setSnackbarSeverity('success');
    } catch (err) {
      setError(err);
      setLoading(false);
      setSnackbarMessage('Error updating inventory');
      setSnackbarSeverity('error');
    }
    setSnackbarOpen(true);
    setIsInventoryModalOpen(false);
    setLoading(false);
  };

  const handleChangeRowsPerPage = (event) => {
    setPage(0);
    setRowsPerPage(parseInt(event.target.value, 10));
  };

  const handleFilterByName = (event) => {
    setPage(0);
    setFilterName(event.target.value);
  };

  const handleCloseInventoryModal = () => {
    setIsInventoryModalOpen(false);
  };

  const handleOpenInventoryModal = () => {
    setIsInventoryModalOpen(true);
  };

  const handleSnackbarClose = () => {
    setSnackbarOpen(false);
  };

  const dataFiltered = applyFilter({
    inputData: inventory,
    comparator: getComparator(order, orderBy),
    filterName,
  });
  const notFound = !dataFiltered.length && !!filterName;

  const renderContent = () => {
    if (loading) {
      return <div>Loading...</div>;
    }

    if (error) {
      return <div>Error: {error.message}</div>;
    }

    return (
      <Container>
        <GeneralEditModal
          open={isInventoryModalOpen}
          handleClose={handleCloseInventoryModal}
          messageHeading="Create Inventory"
          // data={{ name, location, owner, inventoryManager }}
          schema={inventorySchema}
          dropdownOptions={dropdownOptions}
          handleSave={handleNewInventory}
        />

        <Stack direction="row" alignItems="center" justifyContent="space-between" mb={5}>
          <Typography variant="h4">Inventories</Typography>

          <Button
            variant="contained"
            color="inherit"
            startIcon={<Iconify icon="eva:plus-fill" />}
            onClick={handleOpenInventoryModal}
          >
            New Inventory
          </Button>
        </Stack>
        <Card>
          <UserTableToolbar
            numSelected={selected.length}
            filterName={filterName}
            onFilterName={handleFilterByName}
          />
          <Scrollbar>
            <TableContainer sx={{ overflow: 'unset' }}>
              <Table sx={{ minWidth: 800 }}>
                <UserTableHead
                  order={order}
                  orderBy={orderBy}
                  rowCount={inventory.length}
                  numSelected={selected.length}
                  onRequestSort={handleSort}
                  onSelectAllClick={handleSelectAllClick}
                  headLabel={[
                    { id: 'name', label: 'Name' },
                    { id: 'location', label: 'Location' },
                    { id: 'owner', label: 'Owner' },
                    { id: 'inventoryManager', label: 'Inventory Manager' },
                    { id: '' },
                  ]}
                />
                <TableBody>
                  {dataFiltered
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row) => (
                      <InventoryTableRow
                        key={row._id}
                        name={row.name}
                        location={row.location}
                        owner={row.owner}
                        // inventoryManager={row.inventoryManager}
                        id={row._id}
                        users={row.users}
                        selected={selected.indexOf(row.name) !== -1}
                        handleClick={(event) => handleClick(event, row.name)}
                        fetchData={fetchData}
                        token={token}
                        dropdownOptions={dropdownOptions}
                      />
                    ))}
                  <TableEmptyRows
                    height={77}
                    emptyRows={emptyRows(page, rowsPerPage, inventory.length)}
                  />
                  {notFound && <TableNoData query={filterName} />}
                </TableBody>
              </Table>
            </TableContainer>
          </Scrollbar>
          <TablePagination
            page={page}
            component="div"
            count={inventory.length}
            rowsPerPage={rowsPerPage}
            onPageChange={handleChangePage}
            rowsPerPageOptions={[5, 10, 25]}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
          <CustomSnackbar
            open={snackbarOpen}
            onClose={handleSnackbarClose}
            message={snackbarMessage}
            severity={snackbarSeverity}
          />
        </Card>
      </Container>
    );
  };

  return <Container>{renderContent()}</Container>;
}
