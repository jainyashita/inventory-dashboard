import PropTypes from 'prop-types';
import React, { useState } from 'react';

import {
  Button,
  Dialog,
  TextField,
  DialogTitle,
  DialogContent,
  DialogActions,
} from '@mui/material';

export default function EditInventoryModal({ open, handleClose, inventory, handleSave }) {
  const [formData, setFormData] = useState({
    name: inventory.name,
    location: inventory.location,
    owner: inventory.owner,
    inventoryManager: inventory.inventoryManager,
  });

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmit = (data) => {
    handleSave(data);
  };

  return (
    <Dialog open={open} onClose={handleClose}>
      <DialogTitle>Edit Inventory</DialogTitle>
      <DialogContent>
        <TextField
          autoFocus
          margin="dense"
          name="name"
          label="Name"
          type="text"
          fullWidth
          value={formData.name}
          onChange={handleChange}
        />
        <TextField
          margin="dense"
          name="location"
          label="Location"
          type="text"
          fullWidth
          value={formData.location}
          onChange={handleChange}
        />
        <TextField
          margin="dense"
          name="owner"
          label="Owner"
          type="text"
          fullWidth
          value={formData.owner}
          onChange={handleChange}
        />
        <TextField
          margin="dense"
          name="inventoryManager"
          label="Inventory Manager"
          type="text"
          fullWidth
          value={formData.inventoryManager}
          onChange={handleChange}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Cancel</Button>
        <Button onClick={handleSubmit} color="primary">
          Save
        </Button>
      </DialogActions>
    </Dialog>
  );
}

EditInventoryModal.propTypes = {
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  inventory: PropTypes.object.isRequired,
  handleSave: PropTypes.func.isRequired,
};
