import { useState } from 'react';
import PropTypes from 'prop-types';

import Stack from '@mui/material/Stack';
import Popover from '@mui/material/Popover';
import TableRow from '@mui/material/TableRow';
import Checkbox from '@mui/material/Checkbox';
import MenuItem from '@mui/material/MenuItem';
import TableCell from '@mui/material/TableCell';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import { Button, Dialog, DialogTitle, DialogActions, DialogContent } from '@mui/material';

import { UpdateAddress } from 'src/redux/api/user';
import { inventorySchema } from 'src/redux/schema/db-schemas';
import { DeleteInventory, UpdateInventory } from 'src/redux/api/inventory';

import Iconify from 'src/components/iconify';
import GeneralEditModal from 'src/components/form-modal';
import CustomSnackbar from 'src/components/custom-snackbar';

// ----------------------------------------------------------------------

export default function InventoryTableRow({
  selected,
  name,
  location,
  owner,
  dropdownOptions,
  fetchData,
  id,
  users,
  handleClick,
  token,
}) {
  const [open, setOpen] = useState(null);
  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  const [isDeleteDialogOpen, setIsDeleteDialogOpen] = useState(false);
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');
  const [snackbarSeverity, setSnackbarSeverity] = useState('success');
  console.log(users);
  const handleOpenMenu = (event) => {
    setOpen(event.currentTarget);
  };

  const handleCloseMenu = () => {
    setOpen(null);
  };

  const handleOpenEditModal = () => {
    setIsEditModalOpen(true);
    handleCloseMenu();
  };

  const handleCloseEditModal = () => {
    setIsEditModalOpen(false);
  };

  const handleOpenDeleteDialog = () => {
    setIsDeleteDialogOpen(true);
    handleCloseMenu();
  };

  const handleCloseDeleteDialog = () => {
    setIsDeleteDialogOpen(false);
  };
  const handleSnackbarClose = () => {
    setSnackbarOpen(false);
  };

  const handleEditSave = async (formData) => {
    console.log(token, id);
    try {
      const inventoryData = {
        name: formData.name,
        owner: formData.owner,
      };
      const response = await UpdateInventory({ token, id, formData: inventoryData });
      console.log(response);

      const addressData = {
        addressLine1: formData.addressLine1,
        addressLine2: formData.addressLine2,
        landmark: formData.landmark,
        city: formData.city,
        country: formData.country,
        postalCode: formData.postalCode,
      };
      const addressResponse = await UpdateAddress({
        token,
        id: location._id,
        formData: addressData,
      });
      console.log(addressResponse);

      // Close the modal after saving
      setSnackbarMessage('Update successful');
      setSnackbarSeverity('success');
      fetchData();
    } catch (error) {
      console.error('Error updating inventory:', error);
      setSnackbarMessage('Error updating inventory');
      setSnackbarSeverity('error');
    }
    setSnackbarOpen(true);
    setIsEditModalOpen(false);
  };

  const handleDeleteSave = async (formData) => {
    try {
      const response = await DeleteInventory({ token, id });
      console.log(response);
      setSnackbarMessage('Inventory deleted successfully');
      setSnackbarSeverity('success');
      fetchData();
    } catch (error) {
      console.error('Error deleting inventory:', error);
      setSnackbarMessage('Error deleting inventory');
      setSnackbarSeverity('error');
    }
    setSnackbarOpen(true);
    handleCloseDeleteDialog(false);
  };

  return (
    <>
      <TableRow hover tabIndex={-1} role="checkbox" selected={selected}>
        <TableCell padding="checkbox">
          <Checkbox disableRipple checked={selected} onChange={handleClick} />
        </TableCell>

        <TableCell component="th" scope="row" padding="none">
          <Stack direction="row" alignItems="center" spacing={2}>
            <Typography variant="subtitle2" noWrap>
              {name}
            </Typography>
          </Stack>
        </TableCell>

        <TableCell>{`${location?.addressLine1} ${location?.addressLine2} ${location?.landmark} ${location?.city} ${location?.country} ${location?.postalCode}`}</TableCell>

        <TableCell>{`${owner?.FirstName} ${owner?.LastName}`}</TableCell>
        <TableCell>
          {users.length > 0
            ? `${users[0]?.FirstName} ${users[0]?.LastName} (${users[0].mobile})`
            : ''}
        </TableCell>
        <TableCell align="right">
          <IconButton onClick={handleOpenMenu}>
            <Iconify icon="eva:more-vertical-fill" />
          </IconButton>
        </TableCell>
      </TableRow>

      <Popover
        open={!!open}
        anchorEl={open}
        onClose={handleCloseMenu}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        PaperProps={{
          sx: { width: 140 },
        }}
      >
        <MenuItem onClick={handleOpenEditModal}>
          <Iconify icon="eva:edit-fill" sx={{ mr: 2 }} />
          Edit
        </MenuItem>

        <MenuItem onClick={handleOpenDeleteDialog} sx={{ color: 'error.main' }}>
          <Iconify icon="eva:trash-2-outline" sx={{ mr: 2 }} />
          Delete
        </MenuItem>
      </Popover>

      <GeneralEditModal
        messageHeading="Edit Item"
        open={isEditModalOpen}
        handleClose={handleCloseEditModal}
        data={{
          id,
          name,
          owner: owner._id,
          // inventoryManager: inventoryManager._id,
          addressLine1: location.addressLine1,
          addressLine2: location.addressLine2,
          landmark: location.landmark,
          city: location.city,
          country: location.country,
          postalCode: location.postalCode,
        }}
        dropdownOptions={dropdownOptions}
        schema={inventorySchema}
        handleSave={handleEditSave}
      />

      <CustomSnackbar
        open={snackbarOpen}
        onClose={handleSnackbarClose}
        message={snackbarMessage}
        severity={snackbarSeverity}
      />
      <Dialog open={isDeleteDialogOpen} onClose={handleCloseDeleteDialog}>
        <DialogTitle>Confirm Delete</DialogTitle>
        <DialogContent>
          <Typography>Are you sure you want to delete this item?</Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseDeleteDialog} color="primary">
            No
          </Button>
          <Button onClick={handleDeleteSave} color="secondary">
            Yes
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

InventoryTableRow.propTypes = {
  location: PropTypes.any,
  handleClick: PropTypes.func,
  fetchData: PropTypes.func,
  owner: PropTypes.any,
  name: PropTypes.any,
  selected: PropTypes.any,
  users: PropTypes.any,
  dropdownOptions: PropTypes.any,
  id: PropTypes.any,
  token: PropTypes.any,
};
