import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { format, parseISO } from 'date-fns';
import React, { useMemo, useState, useEffect } from 'react';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { DatePicker, LocalizationProvider } from '@mui/x-date-pickers';
import {
  Box,
  Table,
  Paper,
  Stack,
  Button,
  Select,
  Dialog,
  MenuItem,
  TableRow,
  TableHead,
  TableBody,
  TableCell,
  TextField,
  Typography,
  FormControl,
  TableContainer,
  FormHelperText,
  TablePagination,
} from '@mui/material';

import { OrderList, UpdateOrder } from 'src/redux/api/orders';
import { OrderStatus, PaymentMode, TransactionStatus } from 'src/config/enums';

// import CustomSnackbar from 'src/components/custom-snackbar';

import { emptyRows } from 'src/sections/user/utils';
import TableEmptyRows from 'src/sections/user/table-empty-rows';

import OrderInvoice from '../order-invoice';

function OrderPage() {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [orders, setOrders] = useState([]);
  const [editableFields, setEditableFields] = useState({});
  const [validationErrors, setValidationErrors] = useState({});
  const [isInvoiceOpen, setIsInvoiceOpen] = useState(false);
  const [selectedOrder, setSelectedOrder] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  // const [snackbarOpen, setSnackbarOpen] = useState(false);
  // const [snackbarMessage, setSnackbarMessage] = useState('');
  // const [snackbarSeverity, setSnackbarSeverity] = useState('success');

  const [sortOption, setSortOption] = useState('oldestNewest'); // Default sort option
  const [filterOption, setFilterOption] = useState('All'); // Default filter option
  const { token, inventory } = useSelector((state) => state.auth);

  const fetchData = async () => {
    try {
      console.log('data fetched');
      const data = await OrderList(token, inventory);
      setOrders([]);
      setValidationErrors({});
      console.log(data);
      setOrders(data);
      setLoading(false);
    } catch (err) {
      setError(err);
      setLoading(false);
    }
  };
  useEffect(() => {
    fetchData();
  }, [token]);

  const handleSortChange = (event) => {
    setSortOption(event.target.value);
  };

  const handleFilterChange = (event) => {
    setFilterOption(event.target.value);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setPage(0);
    setRowsPerPage(parseInt(event.target.value, 10));
  };
  const sortedAndFilteredOrders = useMemo(() => {
    let sortedOrders = [...orders]; // Start with all orders

    // Apply sorting based on the selected option
    switch (sortOption) {
      case 'newestOldest':
        sortedOrders.sort((a, b) => new Date(b.orderDate) - new Date(a.orderDate));
        break;
      case 'oldestNewest':
        sortedOrders.sort((a, b) => new Date(a.orderDate) - new Date(b.orderDate));
        break;
      default:
        break;
    }

    // Apply filtering based on the selected option
    switch (filterOption) {
      case 'Paid':
        sortedOrders = sortedOrders.filter(
          (order) => order.transactionStatus === TransactionStatus.PAID
        );
        break;
      case 'Unpaid':
        sortedOrders = sortedOrders.filter(
          (order) => order.transactionStatus === TransactionStatus.UNPAID
        );
        break;
      default:
        break;
    }

    return sortedOrders;
  }, [orders, sortOption, filterOption]);

  const handleShowInvoice = (order) => {
    setSelectedOrder(order);
    setIsInvoiceOpen(true);
  };

  const handleCloseInvoice = () => {
    setIsInvoiceOpen(false);
    setSelectedOrder(null);
  };

  const validateFields = (orderId, updatedData) => {
    const errors = {};

    if (updatedData.transactionStatus === TransactionStatus.PAID) {
      if (!updatedData.externalPaymentID) {
        errors.externalPaymentID = 'This field is required';
      }

      if (!updatedData.paymentMode) {
        errors.paymentMode = 'This field is required';
      }

      if (!updatedData.paymentDate) {
        errors.paymentDate = 'This field is required';
      }
      const addError = { [orderId]: errors };

      setValidationErrors((prevErrors) => ({
        ...prevErrors,
        ...addError,
      }));
    }

    console.log('validation errors', validationErrors);
    return Object.keys(errors).length === 0;
  };

  // const handleSnackbarClose = () => {
  //   setSnackbarOpen(false);
  // };

  const handleSave = async (orderId, updatedData) => {
    if (updatedData.paymentDate) {
      updatedData.paymentDate = format(new Date(updatedData.paymentDate), 'yyyy-MM-dd');
    }

    if (!validateFields(orderId, updatedData)) {
      return;
    }

    console.log(validationErrors, 'val errors');
    console.log(`Saving updates for order ${orderId}:`, updatedData);

    try {
      const response = await UpdateOrder({ token, id: orderId, formData: updatedData });
      console.log('Order response:', response);
      // setSnackbarMessage('Update successful');
      // setSnackbarSeverity('success');
      fetchData();
    } catch (err) {
      console.error('Error saving order:', err);
      // setSnackbarMessage('Error saving order');
      // setSnackbarSeverity('error');
    }
    // setSnackbarOpen(true);
  };

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error.message}</div>;
  }
  return (
    <Box m={3} mt={5}>
      <Stack direction="row" justifyContent="space-between" alignItems="center" sx={{ mb: 4 }}>
        <Typography variant="h3">Orders</Typography>
      </Stack>
      <Stack direction="row" justifyContent="flex-end" alignItems="center" sx={{ mb: 6 }}>
        <Box display="flex" alignItems="center">
          <Box display="flex" alignItems="center">
            <Typography variant="subtitle2" sx={{ mr: 2 }}>
              Sort By:
            </Typography>
            <Select
              value={sortOption}
              variant="standard"
              sx={{ minWidth: 120 }}
              onChange={handleSortChange}
            >
              <MenuItem value="newestOldest">Newest to Oldest</MenuItem>
              <MenuItem value="oldestNewest">Oldest to Newest</MenuItem>
            </Select>
          </Box>

          <Box display="flex" alignItems="center" ml={3}>
            <Typography variant="subtitle2" sx={{ mr: 2 }}>
              Filter By:
            </Typography>
            <Select
              value={filterOption}
              variant="standard"
              sx={{ minWidth: 120 }}
              onChange={handleFilterChange}
            >
              <MenuItem value="All">All</MenuItem>
              <MenuItem value="Paid">Paid</MenuItem>
              <MenuItem value="Unpaid">Unpaid</MenuItem>
            </Select>
          </Box>
        </Box>
      </Stack>
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <TableContainer component={Paper}>
          <Table>
            <TableHead>
              <TableRow sx={{ bgcolor: '#f5f5f5' }}>
                {' '}
                {/* Customize the header row */}
                <TableCell sx={{ fontWeight: 'bold', color: 'black' }}>Order Date</TableCell>
                <TableCell sx={{ fontWeight: 'bold', color: 'black' }}>Total Amount</TableCell>
                <TableCell sx={{ fontWeight: 'bold', color: 'black' }}>Order Status</TableCell>
                <TableCell sx={{ fontWeight: 'bold', color: 'black' }}>
                  Transaction Status
                </TableCell>
                <TableCell sx={{ fontWeight: 'bold', color: 'black' }}>
                  External Payment ID
                </TableCell>
                <TableCell sx={{ fontWeight: 'bold', color: 'black' }}>Payment Date</TableCell>
                <TableCell sx={{ fontWeight: 'bold', color: 'black' }}>Payment Mode</TableCell>
                <TableCell sx={{ fontWeight: 'bold', color: 'black' }}>Invoice</TableCell>
                <TableCell sx={{ fontWeight: 'bold', color: 'black' }}>Actions</TableCell>
                <TableCell sx={{ fontWeight: 'bold', color: 'black' }}>Last Updated By</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {sortedAndFilteredOrders
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((order) => (
                  <TableRow key={order.id}>
                    <TableCell>{format(new Date(order.orderDate), 'PPP')}</TableCell>
                    <TableCell>{order.totalAmountWithGst.toFixed(2)}</TableCell>
                    <TableCell>
                      <Select
                        defaultValue={order.orderStatus}
                        onChange={(e) =>
                          setEditableFields({
                            ...editableFields,
                            [order.id]: {
                              ...editableFields[order.id],
                              orderStatus: e.target.value,
                            },
                          })
                        }
                      >
                        {Object.values(OrderStatus).map((statusKey) => (
                          <MenuItem key={statusKey} value={statusKey}>
                            {statusKey.toUpperCase()}
                          </MenuItem>
                        ))}
                      </Select>
                    </TableCell>
                    <TableCell>
                      <Select
                        defaultValue={order.transactionStatus}
                        onChange={(e) =>
                          setEditableFields({
                            ...editableFields,
                            [order.id]: {
                              ...editableFields[order.id],
                              transactionStatus: e.target.value,
                            },
                          })
                        }
                      >
                        {Object.values(TransactionStatus).map((statusKey) => (
                          <MenuItem key={statusKey} value={statusKey}>
                            {statusKey.toUpperCase()}
                          </MenuItem>
                        ))}
                      </Select>
                    </TableCell>
                    <TableCell>
                      <TextField
                        defaultValue={order.externalPaymentID}
                        error={validationErrors[order.id]?.externalPaymentID}
                        helperText={validationErrors[order.id]?.externalPaymentID}
                        onChange={(e) =>
                          setEditableFields({
                            ...editableFields,
                            [order.id]: {
                              ...editableFields[order.id],
                              externalPaymentID: e.target.value,
                            },
                          })
                        }
                      />
                    </TableCell>
                    <TableCell>
                      <DatePicker
                        defaultValue={
                          editableFields[order.id]?.paymentDate ||
                          (order.paymentDate ? parseISO(order.paymentDate) : null)
                        }
                        value={
                          editableFields[order.id]?.paymentDate ||
                          (order.paymentDate ? parseISO(order.paymentDate) : null)
                        }
                        onChange={(date) =>
                          setEditableFields({
                            ...editableFields,
                            [order.id]: {
                              ...editableFields[order.id],
                              paymentDate: date,
                            },
                          })
                        }
                        renderInput={(params) => {
                          let displayValue = '';
                          if (editableFields[order.id]?.paymentDate) {
                            displayValue = format(
                              new Date(editableFields[order.id].paymentDate),
                              'PPP'
                            );
                          } else if (order.paymentDate) {
                            displayValue = format(parseISO(order.paymentDate), 'PPP');
                          }

                          return (
                            <TextField
                              {...params}
                              error={validationErrors[order.id]?.paymentDate}
                              helperText={validationErrors[order.id]?.paymentDate}
                              value={displayValue}
                            />
                          );
                        }}
                      />{' '}
                    </TableCell>
                    <TableCell>
                      <FormControl style={{ minWidth: 100 }}>
                        <Select
                          defaultValue={order.paymentMode}
                          error={validationErrors[order.id]?.paymentMode}
                          onChange={(e) =>
                            setEditableFields({
                              ...editableFields,
                              [order.id]: {
                                ...editableFields[order.id],
                                paymentMode: e.target.value,
                              },
                            })
                          }
                        >
                          {Object.values(PaymentMode).map((statusKey) => (
                            <MenuItem key={statusKey} value={statusKey}>
                              {statusKey.toUpperCase()}
                            </MenuItem>
                          ))}
                        </Select>
                      </FormControl>
                      {validationErrors[order.id]?.paymentMode && (
                        <FormHelperText error>
                          {validationErrors[order.id].paymentMode}
                        </FormHelperText>
                      )}
                    </TableCell>
                    <TableCell>
                      <Button
                        variant="text"
                        color="primary"
                        onClick={() => handleShowInvoice(order)}
                      >
                        View Invoice
                      </Button>
                    </TableCell>
                    <TableCell>
                      <Button
                        variant="contained"
                        color="primary"
                        onClick={() => handleSave(order.id, editableFields[order.id])}
                      >
                        Save
                      </Button>
                    </TableCell>{' '}
                    <TableCell>{`${order.user.FirstName} ${order.user.LastName}`}</TableCell>
                  </TableRow>
                ))}
              <TableEmptyRows height={77} emptyRows={emptyRows(page, rowsPerPage, orders.length)} />
            </TableBody>
          </Table>
        </TableContainer>
      </LocalizationProvider>
      <TablePagination
        page={page}
        component="div"
        count={orders.length}
        rowsPerPage={rowsPerPage}
        onPageChange={handleChangePage}
        rowsPerPageOptions={[5, 10, 25]}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
      {/* <CustomSnackbar
        open={snackbarOpen}
        onClose={handleSnackbarClose}
        message={snackbarMessage}
        severity={snackbarSeverity}
      /> */}
      {selectedOrder && (
        <Dialog open={isInvoiceOpen} onClose={handleCloseInvoice}>
          <OrderInvoice
            currentDate={format(new Date(selectedOrder.orderDate), 'PPP')}
            orderData={selectedOrder}
            token={token}
          />
        </Dialog>
      )}
    </Box>
  );
}

export default OrderPage;

OrderPage.propTypes = {
  location: PropTypes.any,
  handleClick: PropTypes.func,
  owner: PropTypes.any,
  name: PropTypes.any,
  selected: PropTypes.any,
  id: PropTypes.any,
  token: PropTypes.any,
};
