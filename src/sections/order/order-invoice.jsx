import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import 'react-responsive-carousel/lib/styles/carousel.min.css';

import {
  Box,
  List,
  Paper,
  Divider,
  ListItem,
  Typography,
  DialogTitle,
  ListItemText,
  DialogContent,
} from '@mui/material';

import { getAddress } from 'src/redux/api/user';

const OrderInvoice = ({ currentDate, token, orderData }) => {
  const [address, setAddress] = useState('');
  useEffect(() => {
    const getShippingAddress = async () => {
      try {
        const fetchedAddress = await getAddress({ token, id: orderData.address });
        setAddress(
          `${fetchedAddress.addressLine1}, ${fetchedAddress.addressLine2}, ${fetchedAddress.landmark}, ${fetchedAddress.city}, ${fetchedAddress.country}- ${fetchedAddress.postalCode}`
        );
      } catch (error) {
        console.error('Error fetching address:', error);
      }
    };

    if (orderData && orderData?.address) {
      getShippingAddress();
    }
  }, [orderData]);
  return (
    <>
      {' '}
      <DialogTitle sx={{ my: 1 }}>Order Invoice</DialogTitle>
      <DialogContent sx={{ p: 2, pb: 0, minHeight: '300px', minWidth: '600px' }}>
        <Paper elevation={3} sx={{ p: 3, mb: 2 }}>
          <Box sx={{ mb: 2, display: 'flex', justifyContent: 'space-between' }}>
            <Box>
              <Typography variant="h6">Invoice</Typography>
              <Typography>Date: {currentDate}</Typography>
            </Box>
            <Box sx={{ textAlign: 'right' }}>
              <Typography variant="subtitle1">Customer Information</Typography>
              <Typography>Name: {orderData?.customerName}</Typography>
              <Typography>Contact: {orderData?.customerMobile}</Typography>
            </Box>
          </Box>
          <Divider sx={{ mb: 2 }} />
          <Box sx={{ mb: 2 }}>
            <Typography variant="subtitle1">Shipping Address</Typography>
            <Typography>{address}</Typography>
          </Box>
          <Divider sx={{ mb: 2 }} />
          <List>
            {orderData.products.map((item) => (
              <ListItem key={item.id} sx={{ py: 1, mt: 0.5 }}>
                <ListItemText
                  primary={item.name}
                  secondary={
                    <>
                      <Typography component="span">
                        Quantity: {item?.quantity} | Unit Price: ₹{item?.price.toFixed(2)} | Total:
                        ₹{item?.totalPrice.toFixed(2)}
                      </Typography>
                      <Typography component="span" sx={{ display: 'block' }}>
                        GST ({item?.gstRate}%): ₹{item?.gstAmount.toFixed(2)}
                      </Typography>
                      <Typography component="span" sx={{ display: 'block' }}>
                        Total with GST: ₹{item?.totalPriceWithGst.toFixed(2)}
                      </Typography>
                    </>
                  }
                />
              </ListItem>
            ))}
          </List>
          <Divider sx={{ mt: 2 }} />
          <Typography variant="h6" sx={{ mt: 2, textAlign: 'right' }}>
            Total Amount: ₹{orderData?.totalAmountWithGst.toFixed(2)}
          </Typography>
        </Paper>
      </DialogContent>
    </>
  );
};

export default OrderInvoice;

OrderInvoice.propTypes = {
  orderData: PropTypes.any,
  currentDate: PropTypes.any,
  token: PropTypes.any,
};
