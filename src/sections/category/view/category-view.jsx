import { useSelector } from 'react-redux';
import { useState, useEffect } from 'react';

import { Box } from '@mui/system';
import Stack from '@mui/material/Stack';
import Grid from '@mui/material/Unstable_Grid2';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import { Button, CircularProgress } from '@mui/material';

import { GstSlabList } from 'src/redux/api/gst-slab';
import { categorySchema } from 'src/redux/schema/db-schemas';
import { NewCategory, CategoryList } from 'src/redux/api/category';

import Iconify from 'src/components/iconify';
import GeneralEditModal from 'src/components/form-modal';

import CategoryCard from '../category-card';
import ProductSort from '../../products/product-sort';
import ProductFilters from '../../products/product-filters';

// ----------------------------------------------------------------------

export default function CategoryView() {
  const [openFilter, setOpenFilter] = useState(false);
  const [categories, setCategories] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [isProductModalOpen, setIsProductModalOpen] = useState(false);
  const [dropdownOptions, setDropdownOptions] = useState({});
  const { token, inventory, user } = useSelector((state) => state.auth);
  console.log(inventory, 'inventory from category-view');
  const fetchCategories = async () => {
    try {
      const data = await CategoryList(token, inventory);
      setCategories(data.categories);
      setLoading(false);
    } catch (err) {
      console.error('Failed to fetch products', err);
      setLoading(false);
    }
  };
  const fetchGstSlabs = async () => {
    try {
      const slabs = await GstSlabList(token);
      setDropdownOptions((prevState) => ({
        ...prevState,
        gstSlab: slabs.map((slab) => ({ label: slab.name, value: slab._id })),
      }));
    } catch (err) {
      console.error('Failed to fetch GST slabs', err);
    }
  };

  useEffect(() => {
    fetchCategories();
    fetchGstSlabs();
  }, [token, inventory]);

  const handleOpenFilter = () => {
    setOpenFilter(true);
  };

  const handleCloseFilter = () => {
    setOpenFilter(false);
  };

  const handleNewCategory = async (data) => {
    try {
      const response = await NewCategory({
        token,
        data: { ...data, owner: user._id, inventory },
      });
      setCategories((prevCategories) => [...prevCategories, response]);
      fetchCategories();
    } catch (err) {
      setError(err);
    }
    setIsProductModalOpen(false);
    setLoading(false);
  };

  const handleCloseProductModal = () => {
    setIsProductModalOpen(false);
  };

  const handleOpenProductModal = () => {
    console.log(dropdownOptions);
    setIsProductModalOpen(true);
  };

  if (loading) {
    return (
      <Box
        sx={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          minHeight: '200px',
        }}
      >
        <CircularProgress />
      </Box>
    );
  }

  if (error) {
    return <div>Error: {error.message}</div>;
  }
  return (
    <Container>
      {' '}
      <GeneralEditModal
        open={isProductModalOpen}
        handleClose={handleCloseProductModal}
        messageHeading="Create Category"
        schema={categorySchema}
        dropdownOptions={dropdownOptions}
        handleSave={handleNewCategory}
      />
      <Stack direction="row" alignItems="center" justifyContent="space-between" mb={5}>
        <Typography variant="h4" sx={{ mb: 5 }}>
          Manage / Select Categories
        </Typography>
        <Button
          variant="contained"
          color="inherit"
          startIcon={<Iconify icon="eva:plus-fill" />}
          onClick={handleOpenProductModal}
        >
          New Category
        </Button>
      </Stack>
      <Stack
        direction="row"
        alignItems="center"
        flexWrap="wrap-reverse"
        justifyContent="flex-end"
        sx={{ mb: 5 }}
      >
        <Stack direction="row" spacing={1} flexShrink={0} sx={{ my: 1 }}>
          <ProductFilters
            openFilter={openFilter}
            onOpenFilter={handleOpenFilter}
            onCloseFilter={handleCloseFilter}
          />

          <ProductSort />
        </Stack>
      </Stack>
      <Grid container spacing={3}>
        {categories.map((category) => (
          <Grid key={category.id} xs={12} sm={6} md={3}>
            <CategoryCard
              category={category}
              dropdownOptions={dropdownOptions}
              fetchCategories={fetchCategories}
            />
          </Grid>
        ))}
      </Grid>{' '}
    </Container>
  );
}
