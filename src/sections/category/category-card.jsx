import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';

import { Edit as EditIcon, Delete as DeleteIcon } from '@mui/icons-material';
import {
  Card,
  Stack,
  Button,
  Dialog,
  CardMedia,
  IconButton,
  Typography,
  CardContent,
  DialogTitle,
  DialogActions,
  DialogContent,
} from '@mui/material';

import { useRouter } from 'src/routes/hooks';

import { categorySchema } from 'src/redux/schema/db-schemas';
import { DeleteCategory, UpdateCategory } from 'src/redux/api/category';

import GeneralEditModal from 'src/components/form-modal';
import CustomSnackbar from 'src/components/custom-snackbar';

import UploadCategoryImageFunction from './upload-category-image';

const CategoryCard = ({ category, dropdownOptions, fetchCategories }) => {
  const history = useRouter();
  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  const [isDeleteDialogOpen, setIsDeleteDialogOpen] = useState(false);
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');
  const [snackbarSeverity, setSnackbarSeverity] = useState('success');
  const { token } = useSelector((state) => state.auth);

  console.log(category, 'category');
  const handleCardClick = () => {
    history.push(`/category/${category._id}/products/`);
  };

  const handleOpenEditModal = (event) => {
    console.log('in edit modal');
    event.stopPropagation();
    setIsEditModalOpen(true);
  };

  const handleCloseEditModal = () => {
    setIsEditModalOpen(false);
  };

  const handleOpenDeleteDialog = (event) => {
    event.stopPropagation();
    setIsDeleteDialogOpen(true);
  };

  const handleCloseDeleteDialog = () => {
    setIsDeleteDialogOpen(false);
  };
  const handleSnackbarClose = () => {
    setSnackbarOpen(false);
  };
  const handleImageUpload = (updatedProduct) => {
    fetchCategories();
    console.log(updatedProduct);
  };

  const handleEditSave = async (formData) => {
    try {
      const categoryData = {
        name: formData.name,
        desc: formData.desc,
        gstSlab: formData.gstSlab,
      };
      // Make an API call to save the data
      const response = await UpdateCategory({ token, id: category._id, formData: categoryData });
      console.log(response);
      // Close the modal after saving
      setSnackbarMessage('Update successful');
      setSnackbarSeverity('success');
      fetchCategories();
    } catch (error) {
      console.error('Error updating user:', error);
      setSnackbarMessage('Error updating user');
      setSnackbarSeverity('error');
    }
    setSnackbarOpen(true);
    setIsEditModalOpen(false);
  };

  const handleDeleteSave = async (formData) => {
    console.log(token, category._id);
    try {
      const response = await DeleteCategory({ token, id: category._id });
      console.log(response);
      setSnackbarMessage('Category deleted successfully');
      setSnackbarSeverity('success');
      fetchCategories();
    } catch (error) {
      console.error('Error deleting category:', error);
      setSnackbarMessage('Error deleting category');
      setSnackbarSeverity('error');
    }
    setSnackbarOpen(true);
    handleCloseDeleteDialog(false);
  };

  return (
    <>
      <Card
        sx={{
          maxWidth: 345,
          height: '100%', // Ensure each card has equal height
          display: 'flex', // Use flexbox to control layout
          flexDirection: 'column', // Stack content vertically
          boxShadow: 3,
          borderRadius: 2,
          cursor: 'pointer',
          transition: 'transform 0.3s ease-in-out',
          '&:hover': {
            transform: 'scale(1.05)',
          },
        }}
        onClick={handleCardClick}
      >
        <CardMedia
          component="img"
          height="200"
          image={category?.imageURL}
          alt={category.name}
          sx={{ objectFit: 'cover' }}
        />
        <UploadCategoryImageFunction id={category._id} onImageUpload={handleImageUpload} />

        <CardContent sx={{ backgroundColor: '#f5f5f5', flexGrow: 1 }}>
          <Typography gutterBottom variant="body1" component="div">
            {category.name}
          </Typography>

          <Stack direction="row" spacing={1} justifyContent="flex-end">
            <IconButton onClick={handleOpenEditModal}>
              {' '}
              <EditIcon color="secondary" />
            </IconButton>
            <IconButton onClick={handleOpenDeleteDialog}>
              <DeleteIcon color="error" />
            </IconButton>
          </Stack>
        </CardContent>
      </Card>
      <GeneralEditModal
        messageHeading="Edit Category"
        open={isEditModalOpen}
        handleClose={handleCloseEditModal}
        data={{
          id: category._id,
          name: category.name,
          desc: category.desc,
          gstSlab: category.gstSlab,
        }}
        dropdownOptions={dropdownOptions}
        schema={categorySchema}
        handleSave={handleEditSave}
      />{' '}
      <CustomSnackbar
        open={snackbarOpen}
        onClose={handleSnackbarClose}
        message={snackbarMessage}
        severity={snackbarSeverity}
      />
      <Dialog open={isDeleteDialogOpen} onClose={handleCloseDeleteDialog}>
        <DialogTitle>Confirm Delete</DialogTitle>
        <DialogContent>
          <Typography>Are you sure you want to delete this item?</Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseDeleteDialog} color="primary">
            No
          </Button>
          <Button onClick={handleDeleteSave} color="secondary">
            Yes
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

CategoryCard.propTypes = {
  category: PropTypes.shape({
    name: PropTypes.string.isRequired,
    desc: PropTypes.string.isRequired,
    imageURL: PropTypes.arrayOf(PropTypes.string).isRequired,
    _id: PropTypes.any,
    gstSlab: PropTypes.any,
  }).isRequired,
  dropdownOptions: PropTypes.any,
  fetchCategories: PropTypes.func,
};

export default CategoryCard;
