import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';

import { Box, Button, TextField } from '@mui/material';

import { UploadCategoryImage } from 'src/redux/api/category';

const UploadCategoryImageFunction = ({ id, onImageUpload }) => {
  const [file, setFile] = useState(null);
  console.log(id);
  const { token } = useSelector((state) => state.auth);

  const handleFileChange = (e) => {
    setFile(e.target.files[0]);
  };

  const handleUpload = async (event) => {
    event.stopPropagation();
    if (!file) return;
    console.log('this is file upload');
    const formData = new FormData();
    formData.append('categoryImage', file);

    try {
      const response = await UploadCategoryImage({ token, formData, id });
      console.log('uploaded image response', response);
      onImageUpload(response);
    } catch (error) {
      console.error('Failed to upload image', error);
    }
  };

  return (
    <Box display="flex" alignItems="center" sx={{ mt: 4 }}>
      <TextField type="file" onChange={handleFileChange} onClick={(e) => e.stopPropagation()} />
      <Button variant="outlined" sx={{ mx: 2 }} onClick={handleUpload}>
        Upload Image
      </Button>
    </Box>
  );
};

UploadCategoryImageFunction.propTypes = {
  id: PropTypes.any,
  onImageUpload: PropTypes.any,
};

export default UploadCategoryImageFunction;
