import { useState } from 'react';
import PropTypes from 'prop-types';

import Stack from '@mui/material/Stack';
import Popover from '@mui/material/Popover';
import TableRow from '@mui/material/TableRow';
import Checkbox from '@mui/material/Checkbox';
import MenuItem from '@mui/material/MenuItem';
import TableCell from '@mui/material/TableCell';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import { Button, Dialog, DialogTitle, DialogActions, DialogContent } from '@mui/material';

import { userSchema } from 'src/redux/schema/db-schemas';
import { DeleteUser, UpdateUser, UpdateAddress } from 'src/redux/api/user';

import Iconify from 'src/components/iconify';
import GeneralEditModal from 'src/components/form-modal';
import CustomSnackbar from 'src/components/custom-snackbar';

// ----------------------------------------------------------------------

export default function UserTableRow({
  selected,
  username,
  FirstName,
  LastName,
  userType,
  mobile,
  email,
  address,
  inventory,
  dropdownOptions,
  fetchData,
  handleClick,
  id,
  token,
}) {
  const [open, setOpen] = useState(null);
  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  const [isDeleteDialogOpen, setIsDeleteDialogOpen] = useState(false);
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');
  const [snackbarSeverity, setSnackbarSeverity] = useState('success');
  const handleOpenMenu = (event) => {
    setOpen(event.currentTarget);
  };

  const handleCloseMenu = () => {
    setOpen(null);
  };

  const handleOpenEditModal = () => {
    setIsEditModalOpen(true);
    handleCloseMenu();
  };

  const handleCloseEditModal = () => {
    setIsEditModalOpen(false);
  };

  const handleOpenDeleteDialog = () => {
    setIsDeleteDialogOpen(true);
    handleCloseMenu();
  };

  const handleCloseDeleteDialog = () => {
    setIsDeleteDialogOpen(false);
  };
  const handleSnackbarClose = () => {
    setSnackbarOpen(false);
  };

  const handleEditSave = async (formData) => {
    try {
      const userData = {
        username: formData.username,
        FirstName: formData.FirstName,
        LastName: formData.LastName,
        email: formData.email,
        mobile: formData.mobile,
        address: address._id,
        password: formData.password,
        inventory: formData?.inventory || null,
      };
      const userResponse = await UpdateUser({ token, id, formData: userData });

      console.log(userResponse);
      const addressData = {
        addressLine1: formData.addressLine1,
        addressLine2: formData.addressLine2,
        landmark: formData.landmark,
        city: formData.city,
        country: formData.country,
        postalCode: formData.postalCode,
      };
      const addressResponse = await UpdateAddress({
        token,
        id: address._id,
        formData: addressData,
      });
      console.log(addressResponse);

      // Close the modal after saving
      setSnackbarMessage('Update successful');
      setSnackbarSeverity('success');
      fetchData();
    } catch (error) {
      console.error('Error updating user:', error);
      setSnackbarMessage('Error updating user');
      setSnackbarSeverity('error');
    }
    setSnackbarOpen(true);
    setIsEditModalOpen(false);
  };

  const handleDeleteSave = async (formData) => {
    try {
      const response = await DeleteUser({ token, id });
      console.log(response);
      setSnackbarMessage('Inventory deleted successfully');
      setSnackbarSeverity('success');
      fetchData();
    } catch (error) {
      console.error('Error deleting inventory:', error);
      setSnackbarMessage('Error deleting inventory');
      setSnackbarSeverity('error');
    }
    setSnackbarOpen(true);
    handleCloseDeleteDialog(false);
  };

  return (
    <>
      <TableRow hover tabIndex={-1} role="checkbox" selected={selected}>
        <TableCell padding="checkbox">
          <Checkbox disableRipple checked={selected} onChange={handleClick} />
        </TableCell>

        <TableCell component="th" scope="row" padding="none">
          <Stack direction="row" alignItems="center" spacing={2}>
            {/* <Avatar alt={name} src={avatarUrl} /> */}
            <Typography variant="subtitle2" noWrap>
              {username}
            </Typography>
          </Stack>
        </TableCell>

        <TableCell>{FirstName}</TableCell>

        <TableCell>{LastName}</TableCell>

        <TableCell>{userType}</TableCell>
        <TableCell>{mobile}</TableCell>
        <TableCell>{email}</TableCell>
        <TableCell>{`${address.addressLine1} ${address.addressLine2} ${address.landmark} ${address.city} ${address.country} ${address.postalCode}`}</TableCell>
        <TableCell>{inventory && `${inventory?.name}`}</TableCell>
        <TableCell align="right">
          <IconButton onClick={handleOpenMenu}>
            <Iconify icon="eva:more-vertical-fill" />
          </IconButton>
        </TableCell>
      </TableRow>

      <Popover
        open={!!open}
        anchorEl={open}
        onClose={handleCloseMenu}
        anchorOrigin={{ vertical: 'top', horizontal: 'left' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        PaperProps={{
          sx: { width: 140 },
        }}
      >
        <MenuItem onClick={handleOpenEditModal}>
          <Iconify icon="eva:edit-fill" sx={{ mr: 2 }} />
          Edit
        </MenuItem>

        <MenuItem onClick={handleOpenDeleteDialog} sx={{ color: 'error.main' }}>
          <Iconify icon="eva:trash-2-outline" sx={{ mr: 2 }} />
          Delete
        </MenuItem>
      </Popover>
      <GeneralEditModal
        messageHeading="Edit Item"
        open={isEditModalOpen}
        handleClose={handleCloseEditModal}
        data={{
          id,
          username,
          FirstName,
          LastName,
          email,
          mobile,
          userType,
          addressLine1: address.addressLine1,
          addressLine2: address.addressLine2,
          landmark: address.landmark,
          city: address.city,
          country: address.country,
          postalCode: address.postalCode,
        }}
        dropdownOptions={dropdownOptions}
        schema={userSchema}
        handleSave={handleEditSave}
      />
      <CustomSnackbar
        open={snackbarOpen}
        onClose={handleSnackbarClose}
        message={snackbarMessage}
        severity={snackbarSeverity}
      />
      <Dialog open={isDeleteDialogOpen} onClose={handleCloseDeleteDialog}>
        <DialogTitle>Confirm Delete</DialogTitle>
        <DialogContent>
          <Typography>Are you sure you want to delete this item?</Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseDeleteDialog} color="primary">
            No
          </Button>
          <Button onClick={handleDeleteSave} color="secondary">
            Yes
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

UserTableRow.propTypes = {
  username: PropTypes.any,
  FirstName: PropTypes.any,
  handleClick: PropTypes.func,
  LastName: PropTypes.any,
  address: PropTypes.any,
  email: PropTypes.any,
  selected: PropTypes.any,
  mobile: PropTypes.any,
  id: PropTypes.any,
  token: PropTypes.any,
  dropdownOptions: PropTypes.any,
  userType: PropTypes.any,
  fetchData: PropTypes.func,
  inventory: {
    name: PropTypes.any,
  },
};
