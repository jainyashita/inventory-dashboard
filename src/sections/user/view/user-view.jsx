import { useSelector } from 'react-redux';
import { useState, useEffect } from 'react';

import { Box } from '@mui/material';
import Card from '@mui/material/Card';
import Stack from '@mui/material/Stack';
import Table from '@mui/material/Table';
import Button from '@mui/material/Button';
import TableBody from '@mui/material/TableBody';
import Typography from '@mui/material/Typography';
import TableContainer from '@mui/material/TableContainer';
import TablePagination from '@mui/material/TablePagination';

import { UserType } from 'src/config/enums';
import { InventoryList } from 'src/redux/api/inventory';
import { userSchema } from 'src/redux/schema/db-schemas';
import { NewUser, UserList, NewAddress, InventoryWorkersList } from 'src/redux/api/user';

import Iconify from 'src/components/iconify';
import Scrollbar from 'src/components/scrollbar';
import GeneralEditModal from 'src/components/form-modal';

import TableNoData from '../table-no-data';
import UserTableRow from '../user-table-row';
import UserTableHead from '../user-table-head';
import TableEmptyRows from '../table-empty-rows';
import UserTableToolbar from '../user-table-toolbar';
import { emptyRows, applyFilter, getComparator } from '../utils';

// ----------------------------------------------------------------------

export default function UserPage() {
  const [userData, setUserData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [page, setPage] = useState(0);
  const [order, setOrder] = useState('asc');
  const [selected, setSelected] = useState([]);
  const [orderBy, setOrderBy] = useState('name');
  const [filterName, setFilterName] = useState('');
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const { token, user, inventory } = useSelector((state) => state.auth);
  const [isUserModalOpen, setIsUserModalOpen] = useState(false);
  const [dropdownOptions, setDropdownOptions] = useState({
    userType: Object.keys(UserType).map((key) => ({
      value: UserType[key],
      label: key.replace('_', ' '),
    })),
    inventory: [],
  });

  const fetchData = async () => {
    try {
      if (user.userType === 'INVENTORY_MANAGER') {
        const data = await InventoryWorkersList(token, inventory);
        setUserData(data);
      } else {
        const data = await UserList(token);
        setUserData(data);
      }
      setLoading(false);
    } catch (err) {
      setError(err);
      setLoading(false);
    }
  };

  useEffect(() => {
    const fetchInventories = async () => {
      try {
        const inventories = await InventoryList(token); // Assuming fetchGstSlab returns an array of options

        setDropdownOptions((prevState) => ({
          ...prevState,
          inventory: inventories.map((val) => ({
            label: val.name,
            value: val._id,
          })),
        }));
      } catch (err) {
        console.error('Failed to fetch owner', err);
      }
    };
    fetchInventories();
    fetchData();
  }, [token]);

  const handleSort = (event, id) => {
    const isAsc = orderBy === id && order === 'asc';
    if (id !== '') {
      setOrder(isAsc ? 'desc' : 'asc');
      setOrderBy(id);
    }
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = userData.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];
    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setPage(0);
    setRowsPerPage(parseInt(event.target.value, 10));
  };

  const handleFilterByName = (event) => {
    setPage(0);
    setFilterName(event.target.value);
  };

  const handleNewUser = async (data) => {
    try {
      const addressData = {
        addressLine1: data.addressLine1,
        addressLine2: data.addressLine2,
        landmark: data.landmark,
        city: data.city,
        country: data.country,
        postalCode: data.postalCode,
      };
      const addressResponse = await NewAddress({
        token,
        data: addressData,
      });
      const userVal = {
        address: addressResponse._id,
        username: data.username,
        FirstName: data.FirstName,
        LastName: data.LastName,
        userType: data.userType,
        mobile: data.mobile,
        email: data.email,
        password: data.password,
        ...(data.userType !== 'ADMIN' && { inventory: data.inventory }),
      };
      const response = await NewUser({ token, data: userVal });
      console.log(response);

      fetchData();
      setLoading(false);
    } catch (err) {
      setError(err);
      setLoading(false);
    }
    setIsUserModalOpen(false);
  };

  const handleCloseUserModal = () => {
    setIsUserModalOpen(false);
  };

  const handleOpenUserModal = () => {
    setIsUserModalOpen(true);
  };

  const dataFiltered = applyFilter({
    inputData: userData,
    comparator: getComparator(order, orderBy),
    filterName,
  });

  const notFound = !dataFiltered.length && !!filterName;

  const renderContent = () => {
    if (loading) {
      return <div>Loading...</div>;
    }

    if (error) {
      return <div>Error: {error.message}</div>;
    }
    return (
      <Box sx={{ m: 10, mt: 5 }}>
        <GeneralEditModal
          open={isUserModalOpen}
          handleClose={handleCloseUserModal}
          messageHeading="Create User"
          schema={userSchema}
          handleSave={handleNewUser}
          dropdownOptions={dropdownOptions}
        />
        <Stack direction="row" alignItems="center" justifyContent="space-between" mb={5}>
          <Typography variant="h4">
            {' '}
            {user.userType === 'INVENTORY_MANAGER' ? 'Workers' : 'Users'}
          </Typography>

          <Button
            variant="contained"
            color="inherit"
            startIcon={<Iconify icon="eva:plus-fill" />}
            onClick={handleOpenUserModal}
          >
            New User
          </Button>
        </Stack>

        <Card>
          <UserTableToolbar
            numSelected={selected.length}
            filterName={filterName}
            onFilterName={handleFilterByName}
          />

          <Scrollbar>
            <TableContainer sx={{ overflow: 'unset' }}>
              <Table sx={{ minWidth: 1000 }}>
                <UserTableHead
                  order={order}
                  orderBy={orderBy}
                  rowCount={userData.length}
                  numSelected={selected.length}
                  onRequestSort={handleSort}
                  onSelectAllClick={handleSelectAllClick}
                  headLabel={[
                    { id: 'username', label: 'username' },
                    { id: 'FirstName', label: 'First Name' },
                    { id: 'LastName', label: 'Last Name' },
                    { id: 'userType', label: 'User Type' },
                    { id: 'mobile', label: 'Contact No' },
                    { id: 'email', label: 'E-Mail' },
                    { id: 'address', label: 'Address' },
                    { id: 'inventory', label: 'Inventory' },
                    { id: '' },
                  ]}
                />
                <TableBody>
                  {dataFiltered
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row) => (
                      <UserTableRow
                        key={row.id}
                        username={row.username}
                        FirstName={row.FirstName}
                        LastName={row.LastName}
                        userType={row.userType}
                        mobile={row.mobile}
                        email={row.email}
                        address={row.address}
                        inventory={row?.inventory}
                        dropdownOptions={dropdownOptions}
                        id={row._id}
                        fetchData={fetchData}
                        selected={selected.indexOf(row.name) !== -1}
                        handleClick={(event) => handleClick(event, row.name)}
                        token={token}
                      />
                    ))}

                  <TableEmptyRows
                    height={77}
                    emptyRows={emptyRows(page, rowsPerPage, userData.length)}
                  />

                  {notFound && <TableNoData query={filterName} />}
                </TableBody>
              </Table>
            </TableContainer>
          </Scrollbar>

          <TablePagination
            page={page}
            component="div"
            count={userData.length}
            rowsPerPage={rowsPerPage}
            onPageChange={handleChangePage}
            rowsPerPageOptions={[5, 10, 25]}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Card>
      </Box>
    );
  };

  return <>{renderContent()}</>;
}
