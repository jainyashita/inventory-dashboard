import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Card, Grid, Button, CardMedia, Typography, CardContent, CardActions } from '@mui/material';

import { useRouter } from 'src/routes/hooks';

import { getGstSlab } from 'src/redux/api/gst-slab';
import { getCategory } from 'src/redux/api/category';
import { addItem } from 'src/redux/reducers/cartSlice';

const ProductCard = ({ product }) => {
  const { token } = useSelector((state) => state.auth);
  const items = useSelector((state) => state.cart.items);
  const history = useRouter();
  const dispatch = useDispatch();

  const [isInCart, setIsInCart] = useState(false);

  useEffect(() => {
    const inCart = items.some((item) => item.id === product._id);
    setIsInCart(inCart);
  }, [items, product._id]);

  const handleCardClick = () => {
    history.push(`/product/${product._id}`);
  };

  const handleAddToCart = async (event) => {
    event.stopPropagation();
    try {
      const response = await getCategory({
        token,
        id: product.category,
      });
      const gstSlabId = response.gstSlab;
      const gst = await getGstSlab({
        token,
        id: gstSlabId,
      });
      const gstRate = gst.rate;
      dispatch(addItem({ id: product._id, name: product.name, price: product.price, gstRate }));
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <Card
      sx={{
        maxWidth: 345,
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        boxShadow: 3,
        borderRadius: 2,
        cursor: 'pointer',
        transition: 'transform 0.3s ease-in-out',
        '&:hover': {
          transform: 'scale(1.05)',
        },
      }}
      onClick={handleCardClick}
    >
      <CardMedia
        component="img"
        height="200"
        image={product?.imageURL[0]}
        alt={product.name}
        sx={{ objectFit: 'cover' }}
      />
      <CardContent sx={{ backgroundColor: '#f5f5f5', flexGrow: 1, mb: 0 }}>
        <Typography gutterBottom variant="body1" component="div">
          {product.name}
        </Typography>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <Typography variant="subtitle1" color="primary">
              ₹{product.price}
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant="body2" color={product.quantity <= 0 ? 'error' : 'text.secondary'}>
              {product.quantity <= 0 ? 'Out of Stock' : `In Stock: ${product.quantity}`}
            </Typography>
          </Grid>
        </Grid>
      </CardContent>
      {product.quantity > 0 && (
        <CardActions sx={{ width: '100%', backgroundColor: '#f5f5f5' }}>
          <Button
            variant="contained"
            fullWidth
            aria-label={`Add ${product.name} to cart`}
            sx={{
              backgroundColor: isInCart ? 'rgba(76, 175, 80, 1)' : 'rgba(145, 158, 171, 0.12)',
              color: isInCart ? '#fff' : '#000',
              '&:hover': {
                backgroundColor: isInCart ? 'rgba(56, 142, 60, 1)' : 'rgba(255, 255, 255, 1)',
              },
              transition: 'background-color 0.3s ease-in-out, transform 0.3s ease-in-out',
              transform: isInCart ? 'scale(1.1)' : 'none',
              '&.Mui-disabled': {
                backgroundColor: isInCart ? 'rgba(76, 175, 80, 1)' : 'rgba(145, 158, 171, 0.12)',
                color: isInCart ? '#fff' : '#000',
              },
            }}
            disabled={isInCart}
            onClick={handleAddToCart}
          >
            {isInCart ? 'Added to Cart' : 'Add to Cart'}
          </Button>
        </CardActions>
      )}
    </Card>
  );
};

ProductCard.propTypes = {
  product: PropTypes.shape({
    name: PropTypes.string.isRequired,
    desc: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    quantity: PropTypes.number.isRequired,
    imageURL: PropTypes.arrayOf(PropTypes.string).isRequired,
    _id: PropTypes.any,
    category: PropTypes.any,
  }).isRequired,
};

export default ProductCard;
