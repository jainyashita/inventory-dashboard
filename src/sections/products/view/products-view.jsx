import { useSelector } from 'react-redux';
import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';

import Stack from '@mui/material/Stack';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Unstable_Grid2';
import Typography from '@mui/material/Typography';
import { Button, Pagination } from '@mui/material';

import { productSchema } from 'src/redux/schema/db-schemas';
import { NewProduct, ProductList } from 'src/redux/api/product';

import Iconify from 'src/components/iconify';
import CartModal from 'src/components/cart-modal';
import GeneralEditModal from 'src/components/form-modal';

import ProductCard from '../product-card';
import ProductSort from '../product-sort';
import ProductFilters from '../product-filters';
import ProductCartWidget from '../product-cart-widget';

// ----------------------------------------------------------------------

export default function ProductsView() {
  const { id } = useParams();
  const [openFilter, setOpenFilter] = useState(false);
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [isProductModalOpen, setIsProductModalOpen] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const { token } = useSelector((state) => state.auth);
  const [isCartOpen, setIsCartOpen] = useState(false);
  const fetchProducts = async (page) => {
    try {
      const data = await ProductList(id, token, page, 8);
      setTotalPages(data.totalPages);
      setProducts(data.products);
      setLoading(false);
    } catch (err) {
      console.error('Failed to fetch products', err);
      setLoading(false);
    }
  };
  useEffect(() => {
    fetchProducts(currentPage);
  }, [token, currentPage]);

  const handleOpenFilter = () => {
    setOpenFilter(true);
  };

  const handleCloseFilter = () => {
    setOpenFilter(false);
  };

  const handleNewProduct = async (formData) => {
    try {
      const data = {
        ...formData,
        category: id,
      };
      const response = await NewProduct({ token, data });
      fetchProducts();
      console.log(response);
    } catch (err) {
      setError(err);
    }
    setIsProductModalOpen(false);
    setLoading(false);
  };

  const handleCloseProductModal = () => {
    setIsProductModalOpen(false);
  };

  const handleOpenProductModal = () => {
    setIsProductModalOpen(true);
  };
  const handlePageChange = (event, value) => {
    setCurrentPage(value);
  };
  const toggleCart = () => {
    setIsCartOpen(!isCartOpen);
  };
  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error.message}</div>;
  }
  return (
    <Container>
      {' '}
      <GeneralEditModal
        open={isProductModalOpen}
        handleClose={handleCloseProductModal}
        messageHeading="Create Product"
        schema={productSchema}
        handleSave={handleNewProduct}
      />
      <Stack direction="row" alignItems="center" justifyContent="space-between" mb={5}>
        <Typography variant="h4" sx={{ mb: 5 }}>
          Products
        </Typography>
        <Button
          variant="contained"
          color="inherit"
          startIcon={<Iconify icon="eva:plus-fill" />}
          onClick={handleOpenProductModal}
        >
          New Product
        </Button>
      </Stack>
      <Stack
        direction="row"
        alignItems="center"
        flexWrap="wrap-reverse"
        justifyContent="flex-end"
        sx={{ mb: 5 }}
      >
        <Stack direction="row" spacing={1} flexShrink={0} sx={{ my: 1 }}>
          <ProductFilters
            openFilter={openFilter}
            onOpenFilter={handleOpenFilter}
            onCloseFilter={handleCloseFilter}
          />

          <ProductSort />
        </Stack>
      </Stack>
      <Grid container spacing={3}>
        {products.map((product) => (
          <Grid key={product.id} xs={12} sm={6} md={3}>
            <ProductCard product={product} />
          </Grid>
        ))}
      </Grid>{' '}
      <Stack spacing={2} sx={{ mt: 6, mb: 3 }} alignItems="center">
        <Pagination
          count={totalPages}
          page={currentPage}
          onChange={handlePageChange}
          color="primary"
        />
      </Stack>
      <ProductCartWidget toggleCart={toggleCart} />
      <CartModal open={isCartOpen} handleClose={toggleCart} />
    </Container>
  );
}
