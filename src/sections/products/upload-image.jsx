import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';

import { Box, Button, TextField } from '@mui/material';

import { UploadProductImage } from 'src/redux/api/product';

const UploadImage = ({ id, onImageUpload }) => {
  const [file, setFile] = useState(null);
  const { token } = useSelector((state) => state.auth);

  const handleFileChange = (e) => {
    setFile(e.target.files[0]);
  };

  const handleUpload = async () => {
    if (!file) return;
    const formData = new FormData();
    formData.append('productImage', file);

    try {
      const response = await UploadProductImage({ token, formData, id });
      onImageUpload(response);
    } catch (error) {
      console.error('Failed to upload image', error);
    }
  };

  return (
    <Box display="flex" alignItems="center" sx={{ mt: 4 }}>
      <TextField type="file" onChange={handleFileChange} />
      <Button variant="outlined" sx={{ mx: 2 }} onClick={handleUpload}>
        Upload Image
      </Button>
    </Box>
  );
};

UploadImage.propTypes = {
  id: PropTypes.any,
  onImageUpload: PropTypes.any,
};

export default UploadImage;
