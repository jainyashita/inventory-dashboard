import { useParams } from 'react-router-dom';
import React, { useState, useEffect } from 'react';
import { Carousel } from 'react-responsive-carousel';
import { useSelector, useDispatch } from 'react-redux';
import 'react-responsive-carousel/lib/styles/carousel.min.css';

import {
  Box,
  Grid,
  List,
  Rating,
  Button,
  Dialog,
  ListItem,
  Container,
  Typography,
  DialogTitle,
  ListItemText,
  DialogActions,
  DialogContent,
} from '@mui/material';

import { useRouter } from 'src/routes/hooks';

import { getGstSlab } from 'src/redux/api/gst-slab';
import { getCategory } from 'src/redux/api/category';
import { addItem } from 'src/redux/reducers/cartSlice';
import { productSchema } from 'src/redux/schema/db-schemas';
import { getProduct, DeleteProduct, UpdateProduct } from 'src/redux/api/product';

import Iconify from 'src/components/iconify';
import GeneralEditModal from 'src/components/form-modal';
import CustomSnackbar from 'src/components/custom-snackbar';

import UploadImage from './upload-image';
import RemoveImage from './remove-image';

const ProductDetail = () => {
  const { id } = useParams();
  const [product, setProduct] = useState(null);
  const [isUserModalOpen, setIsUserModalOpen] = useState(false);
  const [isDeleteDialogOpen, setIsDeleteDialogOpen] = useState(false);
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');
  const [snackbarSeverity, setSnackbarSeverity] = useState('success');
  const { token } = useSelector((state) => state.auth);
  const items = useSelector((state) => state.cart.items);

  const dispatch = useDispatch();
  const history = useRouter();

  const [isInCart, setIsInCart] = useState(false);

  useEffect(() => {
    const inCart = items.some((item) => item.id === product?._id);
    setIsInCart(inCart);
  }, [items, product?._id]);

  const handleImage = (updatedProduct) => {
    console.log(updatedProduct);
    fetchProduct();
  };

  const fetchProduct = async () => {
    try {
      const response = await getProduct({ token, id });
      console.log(response);
      setProduct(response);
    } catch (error) {
      console.error('Failed to fetch product:', error);
    }
  };
  useEffect(() => {
    fetchProduct();
  }, [token, id]);

  if (!product) {
    return <Typography>Loading...</Typography>;
  }

  const handleCloseUserModal = () => {
    setIsUserModalOpen(false);
  };

  const handleEditSave = async (formData) => {
    try {
      const data = {
        name: formData.name,
        desc: formData.desc,
        SKU: formData.SKU,
        price: formData.price,
        quantity: formData.quantity,
        specifications: formData.specifications,
        rating: formData.rating,
        ROP: formData.ROP,
      };
      const response = await UpdateProduct({ token, id, data });
      console.log(response);
      setSnackbarMessage('Update successful');
      setSnackbarSeverity('success');
      fetchProduct();
    } catch (error) {
      console.error('Error updating user:', error);
      setSnackbarMessage('Error updating user');
      setSnackbarSeverity('error');
    }
    setSnackbarOpen(true);
    setIsUserModalOpen(false);
  };

  const handleDeleteSave = async (formData) => {
    try {
      const response = await DeleteProduct({ token, id });
      console.log(response);
      setSnackbarMessage('Product deleted successfully');
      setSnackbarSeverity('success');
      history.push('/products');
    } catch (error) {
      console.error('Error deleting inventory:', error);
      setSnackbarMessage('Error deleting inventory');
      setSnackbarSeverity('error');
    }
    setSnackbarOpen(true);
    handleCloseDeleteDialog(false);
  };

  const handleOpenUserModal = () => {
    setIsUserModalOpen(true);
  };

  const handleSnackbarClose = () => {
    setSnackbarOpen(false);
  };

  const handleOpenDeleteDialog = () => {
    setIsDeleteDialogOpen(true);
  };

  const handleCloseDeleteDialog = () => {
    setIsDeleteDialogOpen(false);
  };

  const handleAddToCart = async () => {
    try {
      const response = await getCategory({
        token,
        id: product.category,
      });
      const gstSlabId = response.gstSlab;
      const gst = await getGstSlab({
        token,
        id: gstSlabId,
      });
      const gstRate = gst.rate;
      dispatch(addItem({ id: product._id, name: product.name, price: product.price, gstRate }));
    } catch (error) {
      console.log(error);
    }
  };

  const handleBackToPreviousPage = () => {
    history.back();
  };

  return (
    <Container sx={{ mt: 4 }}>
      <GeneralEditModal
        open={isUserModalOpen}
        handleClose={handleCloseUserModal}
        messageHeading="Edit Product"
        data={{
          id,
          name: product.name,
          desc: product.desc,
          SKU: product.SKU,
          price: product.price,
          quantity: product.quantity,
          specifications: product.specifications,
          rating: product.rating,
          ROP: product.ROP,
        }}
        schema={productSchema}
        handleSave={handleEditSave}
      />
      <Grid container spacing={12}>
        <Grid item xs={12} display="flex" justifyContent="space-between" alignItems="center">
          <Button
            variant="text"
            onClick={handleBackToPreviousPage}
            startIcon={<Iconify icon="uil:arrow-left" />}
            // sx={{ mt: 2 }}
          >
            Back to Previous Page
          </Button>{' '}
          <Box>
            <Button
              variant="contained"
              startIcon={<Iconify icon="mdi:cart-outline" />}
              // aria-label={`Add ${product.name} to cart`}
              sx={{
                backgroundColor: 'rgba(255,215,0,0.79)',
                // backgroundColor: isInCart ? 'rgba(76, 175, 80, 1)' : 'rgba(145, 158, 171, 0.12)',
                color: isInCart ? '#fff' : '#000',
                mr: 2,
                '&:hover': {
                  backgroundColor: isInCart ? 'rgba(56, 142, 60, 1)' : 'rgba(255,215,0,1)',
                },
                transition: 'background-color 0.3s ease-in-out, transform 0.3s ease-in-out',
                transform: isInCart ? 'scale(1.1)' : 'none',
                '&.Mui-disabled': {
                  backgroundColor: isInCart ? 'rgba(76, 175, 80, 1)' : 'rgba(145, 158, 171, 0.12)',
                  color: isInCart ? '#fff' : '#000',
                },
              }}
              disabled={product.quantity <= 0 || isInCart}
              onClick={handleAddToCart}
            >
              {isInCart ? <>Added to Cart</> : 'Add to Cart'}
            </Button>{' '}
            <Button
              variant="contained"
              color="inherit"
              startIcon={<Iconify icon="lucide:edit" />}
              sx={{ mr: 2 }}
              onClick={handleOpenUserModal}
            >
              Edit Product
            </Button>{' '}
            <Button
              variant="contained"
              color="error"
              startIcon={<Iconify icon="material-symbols:delete" />}
              onClick={handleOpenDeleteDialog}
            >
              Delete Product
            </Button>
          </Box>
        </Grid>{' '}
        <Grid item xs={12} md={6}>
          {product.imageURL && product.imageURL.length > 0 ? (
            <Carousel showThumbs>
              {product.imageURL.map((url, index) => (
                <div key={index}>
                  <img src={url} alt={product.name} />{' '}
                  <RemoveImage id={id} imageUrl={url} onImageRemove={handleImage} />
                </div>
              ))}
            </Carousel>
          ) : (
            <Typography>No images available</Typography>
          )}
          <UploadImage id={id} onImageUpload={handleImage} />{' '}
          <Box
            sx={{
              color: 'black',
              border: '4px solid #FFDB58',
              padding: '8px',
              borderRadius: '8px',
              display: 'inline-block',
              margin: '8px',
              fontWeight: 'bold',
              marginTop: '25px',
            }}
          >
            ROP : {product.ROP}
          </Box>
        </Grid>
        <Grid item xs={12} md={6}>
          <Typography variant="h3" sx={{ mb: 2 }}>
            {product.name}
          </Typography>
          <Typography variant="body1" sx={{ mb: 3 }}>
            {product.desc}
          </Typography>
          <Typography variant="h4" sx={{ mb: 2 }}>
            Price: ${product.price}
          </Typography>
          <Box sx={{ display: 'flex', alignItems: 'center', mb: 4 }}>
            <Typography variant="h6" sx={{ mr: 1 }}>
              Rating:
            </Typography>
            <Rating value={product.rating} readOnly sx={{ mr: 1 }} />
            {product.rating}{' '}
          </Box>
          <Typography variant="body2" color={product.quantity <= 0 ? 'error' : 'text.secondary'}>
            {product.quantity <= 0 ? 'Out of Stock' : `In Stock: ${product.quantity}`}
          </Typography>
          <Typography variant="body2" sx={{ mb: 2 }}>
            SKU: {product.SKU}
          </Typography>{' '}
          <Typography variant="h6" sx={{ mb: 2, color: 'primary.main', fontWeight: 'bold' }}>
            ROP : {product.ROP}
          </Typography>
          <Typography variant="subtitle1">Specifications:</Typography>
          <List>
            {product.specifications &&
              Object.entries(product.specifications).map(([key, value], index) => (
                <ListItem key={index} sx={{ py: 0 }}>
                  <ListItemText primary={`• ${key}: ${value}`} />
                </ListItem>
              ))}
          </List>
        </Grid>
      </Grid>
      <CustomSnackbar
        open={snackbarOpen}
        onClose={handleSnackbarClose}
        message={snackbarMessage}
        severity={snackbarSeverity}
      />
      <Dialog open={isDeleteDialogOpen} onClose={handleCloseDeleteDialog}>
        <DialogTitle>Confirm Delete</DialogTitle>
        <DialogContent>
          <Typography>Are you sure you want to delete this item?</Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseDeleteDialog} color="primary">
            No
          </Button>
          <Button onClick={handleDeleteSave} color="secondary">
            Yes
          </Button>
        </DialogActions>
      </Dialog>
    </Container>
  );
};

export default ProductDetail;
