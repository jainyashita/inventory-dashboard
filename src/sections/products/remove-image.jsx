import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';

import {
  Box,
  Button,
  Dialog,
  Typography,
  DialogTitle,
  DialogActions,
  DialogContent,
} from '@mui/material';

import { RemoveProductImage } from 'src/redux/api/product';

const RemoveImage = ({ id, imageUrl, onImageRemove }) => {
  const [isDeleteDialogOpen, setIsDeleteDialogOpen] = useState(false);
  const { token } = useSelector((state) => state.auth);

  const handleRemove = async () => {
    try {
      const response = await RemoveProductImage({ token, id, imageUrl });
      onImageRemove(response);
    } catch (error) {
      console.error('Failed to remove image', error);
    }
  };
  const handleOpenDeleteDialog = (event) => {
    event.stopPropagation();
    setIsDeleteDialogOpen(true);
  };

  const handleCloseDeleteDialog = () => {
    setIsDeleteDialogOpen(false);
  };
  return (
    <Box sx={{ position: 'absolute', bottom: '2px', right: '2px' }}>
      <Button
        variant="contained"
        color="error"
        size="small"
        onClick={handleOpenDeleteDialog}
        sx={{
          minWidth: 'auto',
        }}
      >
        delete
      </Button>
      <Dialog open={isDeleteDialogOpen} onClose={handleCloseDeleteDialog}>
        <DialogTitle>Confirm Delete</DialogTitle>
        <DialogContent>
          <Typography>Are you sure you want to delete this item?</Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseDeleteDialog} color="primary">
            No
          </Button>
          <Button onClick={handleRemove} color="secondary">
            Yes
          </Button>
        </DialogActions>
      </Dialog>
    </Box>
  );
};

RemoveImage.propTypes = {
  id: PropTypes.any,
  imageUrl: PropTypes.any,
  onImageRemove: PropTypes.any,
};

export default RemoveImage;
