import PropTypes from 'prop-types';

import Box from '@mui/material/Box';
import { styled } from '@mui/system';
import Card from '@mui/material/Card';
import Alert from '@mui/material/Alert';
import Table from '@mui/material/Table';
import TableRow from '@mui/material/TableRow';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import CardHeader from '@mui/material/CardHeader';
import TableContainer from '@mui/material/TableContainer';
import { Warning as WarningIcon, CheckCircle as CheckCircleIcon } from '@mui/icons-material/';

// Styled component for scrollable table container
const ScrollableTableContainer = styled(TableContainer)(({ theme }) => ({
  maxHeight: 400, // Adjust the height as needed
  overflowY: 'auto',
}));

export default function ProductsBelowROPWidget({ products }) {
  const productsBelowROP = products.filter((product) => product.currentStock < product.rop);
  const allProductsSufficientlyStocked = productsBelowROP.length === 0;

  return (
    <Card
      sx={{
        boxShadow: 'none',
        border: `2px solid ${allProductsSufficientlyStocked ? 'green' : 'red'}`,
      }}
    >
      <CardHeader
        title="Products Below ROP"
        action={
          allProductsSufficientlyStocked ? (
            <CheckCircleIcon sx={{ fontSize: 42, color: 'green' }} />
          ) : (
            <WarningIcon sx={{ fontSize: 42, color: '#ff9800' }} />
          )
        }
      />
      <Box sx={{ p: 3 }}>
        {allProductsSufficientlyStocked ? (
          <Alert severity="success">All products are sufficiently stocked.</Alert>
        ) : (
          <ScrollableTableContainer>
            <Table stickyHeader>
              <TableHead>
                <TableRow>
                  <TableCell>Product Name</TableCell>
                  <TableCell align="right">Current Stock</TableCell>
                  <TableCell align="right">Reorder Point</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {productsBelowROP.map((product) => (
                  <TableRow key={product.id}>
                    <TableCell>{product.name}</TableCell>
                    <TableCell align="right">{product.currentStock}</TableCell>
                    <TableCell align="right">{product.rop}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </ScrollableTableContainer>
        )}
      </Box>
    </Card>
  );
}

ProductsBelowROPWidget.propTypes = {
  products: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      currentStock: PropTypes.number.isRequired,
      rop: PropTypes.number.isRequired,
    })
  ).isRequired,
};
