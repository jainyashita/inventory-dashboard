import { useState } from 'react';
import PropTypes from 'prop-types';

import Box from '@mui/material/Box';
import { styled } from '@mui/system';
import Card from '@mui/material/Card';
import Table from '@mui/material/Table';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import TableRow from '@mui/material/TableRow';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import CardHeader from '@mui/material/CardHeader';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import TableContainer from '@mui/material/TableContainer';
import CircularProgress from '@mui/material/CircularProgress';

import { getCategory } from 'src/redux/api/category';

const ScrollableTableContainer = styled(TableContainer)(({ theme }) => ({
  maxHeight: 400,
  overflowY: 'auto',
}));

export default function StockSummaryTable({ title, subheader, data, token }) {
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [selectedCategory, setSelectedCategory] = useState(null);
  const [productData, setProductData] = useState([]);

  const handleOpen = async (category) => {
    setSelectedCategory(category);
    setOpen(true);
    setLoading(true);

    try {
      const response = await getCategory({ token, id: category.categoryId });
      setProductData(response.products);
      setLoading(false);
    } catch (error) {
      console.error('Failed to fetch category:', error);
      setLoading(false);
    }
  };

  const handleClose = () => {
    setOpen(false);
    setProductData([]);
  };

  return (
    <Card>
      <CardHeader title={title} subheader={subheader} />
      <Box sx={{ p: 3, pb: 1 }}>
        <ScrollableTableContainer>
          <Table stickyHeader>
            <TableHead>
              <TableRow>
                <TableCell>Category</TableCell>
                <TableCell align="right">No. of Products</TableCell>
                <TableCell align="right">No. of Units (In Stock)</TableCell>
                <TableCell align="right">Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data.map((row) => (
                <TableRow key={row.categoryId}>
                  <TableCell>{row.categoryName}</TableCell>
                  <TableCell align="right">{row.productCount}</TableCell>
                  <TableCell align="right">{row.totalUnits}</TableCell>
                  <TableCell align="right">
                    <Button variant="text" onClick={() => handleOpen(row)}>
                      Details
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </ScrollableTableContainer>
      </Box>

      <Dialog open={open} onClose={handleClose} fullWidth maxWidth="sm">
        <DialogTitle>{selectedCategory?.categoryName} - Product Details</DialogTitle>
        <DialogContent dividers>
          {loading ? (
            <Box
              sx={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                minHeight: '200px',
              }}
            >
              <CircularProgress />
            </Box>
          ) : (
            <TableContainer>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Product Name</TableCell>
                    <TableCell align="right">Units (In Stock)</TableCell>
                    <TableCell align="right">Price</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {productData.map((product, index) => (
                    <TableRow key={index}>
                      <TableCell>{product.name}</TableCell>
                      <TableCell align="right">{product.quantity}</TableCell>
                      <TableCell align="right">{product.price}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          )}
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Close</Button>
        </DialogActions>
      </Dialog>
    </Card>
  );
}

StockSummaryTable.propTypes = {
  title: PropTypes.string,
  subheader: PropTypes.string,
  token: PropTypes.string,
  data: PropTypes.arrayOf(
    PropTypes.shape({
      categoryId: PropTypes.string.isRequired,
      categoryName: PropTypes.string.isRequired,
      productCount: PropTypes.number.isRequired,
      totalUnits: PropTypes.number.isRequired,
    })
  ).isRequired,
};
