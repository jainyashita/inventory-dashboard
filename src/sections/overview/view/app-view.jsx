import moment from 'moment';
import { faker } from '@faker-js/faker';
import { useSelector } from 'react-redux';
import { useState, useEffect } from 'react';

import Container from '@mui/material/Container';
import Grid from '@mui/material/Unstable_Grid2';
import Typography from '@mui/material/Typography';
import { Box, CircularProgress } from '@mui/material';

import { fAmountSuffix } from 'src/utils/format-number';

import { UserCount } from 'src/redux/api/user';
import { OrderList, OrderStats } from 'src/redux/api/orders';
import { CategoryListWithProducts } from 'src/redux/api/category';
import { InventoryStock, fetchProductsBelowROP } from 'src/redux/api/product';

import Iconify from 'src/components/iconify';

import AppOrderTimeline from '../app-order-timeline';
import AppCurrentVisits from '../app-current-visits';
import AppWebsiteVisits from '../app-website-visits';
import AppWidgetSummary from '../app-widget-summary';
import AppTrafficBySite from '../app-traffic-by-site';
import ProductsBelowROPWidget from '../app-products-rop';
import StockSummaryTable from '../app-stock-summary-table';
import SalesMetricsWidget from '../app-total-sale-metrics';

// ----------------------------------------------------------------------

export default function AppView() {
  const [productCount, setProductCount] = useState(0);
  const [userCount, setUserCount] = useState(0);
  const [totalSales, setTotalSales] = useState(0);
  const [totalReceivedAmount, setTotalReceivedAmount] = useState(0);
  const [totalDuePayment, setTotalDuePayment] = useState(0);
  const [inventoryStock, setInventoryStock] = useState();
  const [totalCardOrders, setTotalCardOrders] = useState(0);
  const [totalCreditOrders, setTotalCreditOrders] = useState(0);
  const [totalCashOrders, setTotalCashOrders] = useState(0);
  const [totalPaidOrders, setTotalPaidOrders] = useState(0);
  const [totalUnpaidOrders, setTotalUnpaidOrders] = useState(0);
  const [totalUPIOrders, setTotalUPIOrders] = useState(0);
  const [orderCount, setOrderCount] = useState(0);
  const [loading, setLoading] = useState(true);
  const [orderData, setOrderData] = useState();
  const [categoryChartData, setCategoryChartData] = useState();
  const [ProductsBelowROP, setProductsBelowROP] = useState();

  const { token, inventory } = useSelector((state) => state.auth);

  const transformCategoryData = (categoryObj) =>
    categoryObj.map((category) => ({
      label: category.name,
      value: category.productCount,
    }));

  useEffect(() => {
    const fetchAllData = async () => {
      try {
        const id = inventory;
        const stock = await InventoryStock(token, id);
        setProductCount(stock.totalProductCount);
        setInventoryStock(stock.categories);
      } catch (err) {
        console.error('Failed to fetch stock summary', err);
      }

      try {
        const count = await UserCount(token);
        setUserCount(count);
      } catch (err) {
        console.error('Failed to fetch number of users', err);
      }

      try {
        const id = inventory;
        const stats = await OrderStats(token, id);
        setOrderCount(stats.orderCount);
        setTotalSales(stats.totalAmountSales);
        setTotalCardOrders(stats.totalCardOrders);
        setTotalCashOrders(stats.totalCashOrders);
        setTotalUPIOrders(stats.totalUPIOrders);
        setTotalDuePayment(stats.totalDuePayment);
        setTotalReceivedAmount(stats.totalReceivedAmount);
        setTotalPaidOrders(stats.totalPaidOrders);
        setTotalCreditOrders(stats.totalCreditOrders);
        setTotalUnpaidOrders(stats.totalUnpaidOrders);
      } catch (err) {
        console.error('Failed to fetch number of orders', err);
      }
      try {
        const id = inventory;
        const response = await fetchProductsBelowROP(id, token);
        setProductsBelowROP(response);
      } catch (err) {
        console.error('Failed to fetch number of orders', err);
      }
      try {
        const id = inventory;
        const orders = await OrderList(token, id);
        const orderCounts = orders.reduce((acc, order) => {
          const date = moment(order.orderDate).format('MM/DD/YYYY'); // Format date for chart label
          if (acc[date]) {
            acc[date] += 1;
          } else {
            acc[date] = 1;
          }
          return acc;
        }, {});

        // Convert orderCounts object to arrays for labels and series data
        const labels = Object.keys(orderCounts);
        const seriesData = Object.values(orderCounts);

        // Prepare data structure for the chart
        const chartData = {
          labels,
          series: [
            {
              name: 'Orders',
              type: 'column', // Adjust chart type based on your preference
              fill: 'solid',
              data: seriesData,
            },
          ],
        };
        // Set the formatted data to state
        setOrderData(chartData);
      } catch (err) {
        console.error('Failed to fetch number of orders', err);
      }
      try {
        const id = inventory;
        const categoryData = await CategoryListWithProducts(token, id);
        const transformedData = transformCategoryData(categoryData);

        setCategoryChartData(transformedData);
      } catch (err) {
        console.error('Failed to fetch categories', err);
      }
    };

    setLoading(false);
    fetchAllData();
  }, [inventory]);
  if (loading) {
    <Box
      sx={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        minHeight: '200px',
      }}
    >
      <CircularProgress />
    </Box>;
  }
  return (
    <Container maxWidth="xl">
      <Typography variant="h4" sx={{ mb: 5 }}>
        Hi, Welcome back 👋
      </Typography>

      <Grid container spacing={3}>
        <Grid xs={12} sm={6} md={3}>
          <AppWidgetSummary
            title="Total Products"
            total={productCount}
            color="success"
            icon={<img alt="icon" src="/assets/icons/glass/ic_glass_bag.png" />}
          />
        </Grid>
        <Grid xs={12} sm={6} md={3}>
          <AppWidgetSummary
            title="Users"
            total={userCount}
            color="info"
            icon={<img alt="icon" src="/assets/icons/glass/ic_glass_users.png" />}
          />
        </Grid>
        <Grid xs={12} sm={6} md={3}>
          <AppWidgetSummary
            title="Item Orders"
            total={orderCount}
            color="warning"
            icon={<img alt="icon" src="/assets/icons/glass/ic_glass_buy.png" />}
          />
        </Grid>
        <Grid xs={12} sm={6} md={3}>
          <AppWidgetSummary
            title="Bug Reports"
            total={0}
            color="error"
            icon={<img alt="icon" src="/assets/icons/glass/ic_glass_message.png" />}
          />
        </Grid>
        <Grid xs={12} md={6} lg={8}>
          {totalPaidOrders ? (
            <SalesMetricsWidget
              totalGrossSales={totalSales}
              totalPaymentsReceived={totalReceivedAmount}
              totalDuePayments={totalDuePayment}
            />
          ) : (
            <Box
              sx={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                minHeight: '200px',
              }}
            >
              <CircularProgress />
            </Box>
          )}
        </Grid>{' '}
        <Grid xs={12} md={6} lg={4}>
          {orderCount ? (
            <AppOrderTimeline
              title="Order Statistics"
              list={[...Array(3)].map((_, index) => ({
                id: faker.string.uuid(),
                title: [
                  `${orderCount}, orders, ₹${fAmountSuffix(totalSales)}`,
                  `${totalPaidOrders} Invoices have been paid`,
                  `Payment of ${totalUnpaidOrders} Invoices is remaining`,
                ][index],
                type: `order${index + 1}`,
              }))}
            />
          ) : (
            <Box
              sx={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                minHeight: '200px',
              }}
            >
              <CircularProgress />
            </Box>
          )}
        </Grid>{' '}
        <Grid xs={12} md={6} lg={4}>
          {categoryChartData ? (
            <AppCurrentVisits
              title="Categories with Products"
              chart={{ series: categoryChartData }}
            />
          ) : (
            <Box
              sx={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                minHeight: '200px',
              }}
            >
              <CircularProgress />
            </Box>
          )}
        </Grid>{' '}
        <Grid xs={12} md={6} lg={8}>
          {inventoryStock ? (
            <StockSummaryTable
              title="Stock Summary"
              subheader="Overview of stock categories"
              data={inventoryStock}
              token={token}
            />
          ) : (
            <Box
              sx={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                minHeight: '200px',
              }}
            >
              <CircularProgress />
            </Box>
          )}
        </Grid>
        <Grid xs={12} md={6} lg={8}>
          {orderData ? (
            <AppWebsiteVisits
              title="Units ordered forecast"
              subheader="orders with date"
              chart={orderData}
            />
          ) : (
            <Box
              sx={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                minHeight: '200px',
              }}
            >
              <CircularProgress />
            </Box>
          )}
        </Grid>
        <Grid xs={12} md={6} lg={4}>
          {totalCardOrders >= 0 ? (
            <AppTrafficBySite
              title="Modes of Payment"
              list={[
                {
                  name: 'CASH',
                  value: totalCashOrders,
                  icon: (
                    <Iconify icon="flat-color-icons:money-transfer" color="#1877F2" width={32} />
                  ),
                },
                {
                  name: 'UPI',
                  value: totalUPIOrders,
                  icon: (
                    <Iconify icon="fluent-emoji-flat:mobile-phone" color="#DF3E30" width={32} />
                  ),
                },
                {
                  name: 'CARD',
                  value: totalCardOrders,
                  icon: <Iconify icon="icon-park:bank-card" color="#006097" width={32} />,
                },
                {
                  name: 'CREDIT',
                  value: totalCreditOrders,
                  icon: <Iconify icon="noto-v1:credit-card" color="#1C9CEA" width={32} />,
                },
              ]}
            />
          ) : (
            <Box
              sx={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                minHeight: '200px',
              }}
            >
              <CircularProgress />
            </Box>
          )}
        </Grid>
        <Grid xs={12} md={6} lg={8}>
          {' '}
          {ProductsBelowROP ? (
            <ProductsBelowROPWidget products={ProductsBelowROP} />
          ) : (
            <Box
              sx={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                minHeight: '200px',
              }}
            >
              <CircularProgress />
            </Box>
          )}
        </Grid>
      </Grid>
    </Container>
  );
}
