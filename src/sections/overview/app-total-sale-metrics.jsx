import PropTypes from 'prop-types';

import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import Grid from '@mui/material/Grid';
import CardHeader from '@mui/material/CardHeader';
import Typography from '@mui/material/Typography';
import {
  Payment as PaymentIcon,
  MonetizationOn as MonetizationOnIcon,
  AccountBalanceWallet as AccountBalanceWalletIcon,
} from '@mui/icons-material/';

import { fAmountSuffix } from 'src/utils/format-number';

export default function SalesMetricsWidget({
  totalGrossSales,
  totalPaymentsReceived,
  totalDuePayments,
}) {
  return (
    <Card sx={{ backgroundColor: '#f0f0f0', boxShadow: 'none' }}>
      <CardHeader title="Sales Metrics" />
      <Box sx={{ p: 5, mb: 5 }}>
        <Grid container spacing={2}>
          <Grid
            item
            xs={4}
            justifyContent="center"
            sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center', p: 0 }}
          >
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
              <MonetizationOnIcon sx={{ fontSize: 42, color: '#3f51b5', mr: 2 }} />
              <Typography variant="h3">{`₹${fAmountSuffix(totalGrossSales)}`}</Typography>
            </Box>
            <Typography variant="subtitle1" sx={{ color: 'text.secondary', mt: 1 }}>
              Cumulative Gross Sales
            </Typography>
          </Grid>
          <Grid
            item
            xs={4}
            sx={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
              <AccountBalanceWalletIcon sx={{ fontSize: 42, color: '#f50057', mr: 2 }} />
              <Typography variant="h3">{`₹${fAmountSuffix(totalPaymentsReceived)}`}</Typography>
            </Box>
            <Typography variant="subtitle1" sx={{ color: 'text.secondary', mt: 1 }}>
              Total Payments Received
            </Typography>
          </Grid>
          <Grid item xs={4} sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
              <PaymentIcon sx={{ fontSize: 42, color: '#4caf50', mr: 2 }} />
              <Typography variant="h3">{`₹${fAmountSuffix(totalDuePayments)}`}</Typography>
            </Box>
            <Typography variant="subtitle1" sx={{ color: 'text.secondary', mt: 1 }}>
              Total Outstanding Amount
            </Typography>
          </Grid>
        </Grid>
      </Box>
    </Card>
  );
}

SalesMetricsWidget.propTypes = {
  totalGrossSales: PropTypes.number.isRequired,
  totalPaymentsReceived: PropTypes.number.isRequired,
  totalDuePayments: PropTypes.number.isRequired,
};
