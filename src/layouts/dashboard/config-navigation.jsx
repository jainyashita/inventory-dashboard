import SvgColor from 'src/components/svg-color';

// ----------------------------------------------------------------------

const icon = (name) => (
  <SvgColor src={`/assets/icons/navbar/${name}.svg`} sx={{ width: 1, height: 1 }} />
);

const navConfig = [
  {
    title: 'dashboard',
    path: '/',
    icon: icon('ic_analytics'),
  },

  { title: 'inventory', path: '/inventory', roles: ['ADMIN'], icon: icon('ic_inventory') },
  { title: 'Products', path: '/category', icon: icon('ic_cart') },
  {
    title: 'user',
    path: '/user',
    icon: icon('ic_user'),
    roles: ['ADMIN', 'INVENTORY_MANAGER'],
    // subItems: [
    //   {
    //     title: 'Users',
    //     path: '/user',
    //   },
    //   {
    //     title: 'Settings',
    //     path: '/products',
    //   },
    // ],
  },

  {
    title: 'gst slab',
    path: '/gstSlab',
    icon: icon('ic_tax'),
  },
  {
    title: 'order',
    path: '/order',
    icon: icon('ic_order'),
  },
];

export default navConfig;
