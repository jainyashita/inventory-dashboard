import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';

import Box from '@mui/material/Box';
import Stack from '@mui/material/Stack';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import { useTheme } from '@mui/material/styles';
import { Select, MenuItem } from '@mui/material';
import IconButton from '@mui/material/IconButton';

import { useResponsive } from 'src/hooks/use-responsive';

import { bgBlur } from 'src/theme/css';
import { updateInventory } from 'src/redux/reducers/authSlice';
import { setSelectedInventory } from 'src/redux/reducers/inventorySlice';

import Iconify from 'src/components/iconify';

import Searchbar from './common/searchbar';
import { NAV, HEADER } from './config-layout';
import AccountPopover from './common/account-popover';
// import LanguagePopover from './common/language-popover';
import NotificationsPopover from './common/notifications-popover';
// ----------------------------------------------------------------------

export default function Header({ onOpenNav }) {
  const theme = useTheme();
  const dispatch = useDispatch();
  const lgUp = useResponsive('up', 'lg');
  const { user, inventory } = useSelector((state) => state.auth);
  const { list, selected } = useSelector((state) => state.inventory);

  const handleInventoryChange = (event) => {
    const selectedInventoryId = event.target.value;
    dispatch(setSelectedInventory(selectedInventoryId));
    dispatch(updateInventory({ inventory: selectedInventoryId }));
  };
  const renderContent = (
    <>
      {!lgUp && (
        <IconButton onClick={onOpenNav} sx={{ mr: 1 }}>
          <Iconify icon="eva:menu-2-fill" />
        </IconButton>
      )}

      <Searchbar />

      <Box sx={{ flexGrow: 1 }} />

      <Stack direction="row" alignItems="center" spacing={1}>
        {user?.userType === 'ADMIN' && !user.inventory && (
          <Select
            value={selected || inventory}
            onChange={handleInventoryChange}
            displayEmpty
            sx={{
              minWidth: 200,
              '&:hover': {
                borderColor: theme.palette.info.light,
              },
            }}
          >
            <MenuItem disabled value="">
              <em>Select Inventory</em>
            </MenuItem>
            {list.map((inventoryVal) => (
              <MenuItem key={inventoryVal._id} value={inventoryVal._id}>
                {inventoryVal.name}
              </MenuItem>
            ))}
          </Select>
        )}{' '}
        {/* <LanguagePopover /> */}
        <NotificationsPopover />
        <AccountPopover />
      </Stack>
    </>
  );

  return (
    <AppBar
      sx={{
        boxShadow: 'none',
        height: HEADER.H_MOBILE,
        zIndex: theme.zIndex.appBar + 1,
        ...bgBlur({
          color: theme.palette.background.default,
        }),
        transition: theme.transitions.create(['height'], {
          duration: theme.transitions.duration.shorter,
        }),
        ...(lgUp && {
          width: `calc(100% - ${NAV.WIDTH + 1}px)`,
          height: HEADER.H_DESKTOP,
        }),
      }}
    >
      <Toolbar
        sx={{
          height: 1,
          px: { lg: 5 },
        }}
      >
        {renderContent}
      </Toolbar>
    </AppBar>
  );
}

Header.propTypes = {
  onOpenNav: PropTypes.func,
};
