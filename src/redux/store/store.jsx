// src/redux/store.js

import { configureStore } from '@reduxjs/toolkit';

import authReducer from '../reducers/authSlice';
import cartReducer from '../reducers/cartSlice';
import inventoryReducer from '../reducers/inventorySlice';

const store = configureStore({
  reducer: {
    auth: authReducer,
    inventory: inventoryReducer,
    cart: cartReducer,
    // Add other slices as needed
  },
});

export default store;
