import axios from 'axios';

import API_BASE_URL, { endpoints } from '../../config/api';

export async function GstSlabList(token) {
  try {
    const response = await axios.get(`${API_BASE_URL}${endpoints.GSTSLAB}`, {
      headers: {
        Authorization: `Bearer ${token}`, // Include the authorization token in the request headers
      },
    });
    const listValue = response.data.gstSlabObj;

    return listValue;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      throw new Error(errorMessage);
    } else {
      console.error('An error occurred while getting gst slabs:', error);
      throw new Error('An error occurred while getting gst slabs. Please try again.'); // Default error message
    }
  }
}
export async function getGstSlab({ token, id }) {
  try {
    const response = await axios.get(`${API_BASE_URL}${endpoints.GSTSLAB}/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const listValue = response.data.gstSlabObj;

    return listValue;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      console.log(errorMessage);
      throw new Error(errorMessage);
    } else {
      console.error('An error occurred while :', error);
      throw new Error('An error occurred while fetching gst slab. Please try again.');
    }
  }
}

export async function NewGstSlab({ token, data }) {
  try {
    const response = await axios.post(`${API_BASE_URL}${endpoints.GSTSLAB}`, data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const listValue = response.data.gstSlabObj;

    return listValue;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      throw new Error(errorMessage); // Throw an error if login fails
    } else {
      console.error('An error occurred while creating gst slab:', error);
      throw new Error('An error occurred while creating gst slab. Please try again.'); // Default error message
    }
  }
}

export async function UpdateGstSlab({ token, id, formData }) {
  try {
    // Perform API call using Axios
    console.log(`value us this only ${API_BASE_URL}${endpoints.GSTSLAB}/${id}`);
    console.log('formData', formData);
    console.log('from the API', token, id, formData);
    const response = await axios.put(`${API_BASE_URL}${endpoints.GSTSLAB}/${id}`, formData, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const listValue = response.data.gstSlabObj;
    if (listValue) {
      return true;
    }
    return false;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      throw new Error(errorMessage); // Throw an error if login fails
    } else {
      console.error('An error occurred while updatng gst slab:', error);
      throw new Error('An error occurred while updatng gst slab. Please try again.'); // Default error message
    }
  }
}

export async function DeleteGstSlab({ token, id, formData }) {
  try {
    // Perform API call using Axios

    const response = await axios.delete(`${API_BASE_URL}${endpoints.GSTSLAB}/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const listValue = response.data.gstSlabObj;
    if (listValue) {
      return true;
    }
    return false;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      throw new Error(errorMessage); // Throw an error if login fails
    } else {
      console.error('An error occurred while deleting gst slab:', error);
      throw new Error('An error occurred while deleting gst slab. Please try again.'); // Default error message
    }
  }
}
