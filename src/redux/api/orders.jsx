import axios from 'axios';

import API_BASE_URL, { endpoints } from 'src/config/api';

export async function OrderStats(token, id) {
  try {
    const response = await axios.get(`${API_BASE_URL}${endpoints.ORDER}/${id}${endpoints.STATS}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const listValue = response.data.orderParameters;

    return listValue;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      throw new Error(errorMessage); // Throw an error if login fails
    } else {
      console.error('An error occurred while getting Orders count', error);
      throw new Error('An error occurred while getting Orders count. Please try again.'); // Default error message
    }
  }
}

export async function OrderList(token, id) {
  try {
    // Perform API call using Axios

    const response = await axios.get(
      `${API_BASE_URL}${endpoints.INVENTORYLIST}/${id}${endpoints.ORDER}`,
      {
        headers: {
          Authorization: `Bearer ${token}`, // Include the authorization token in the request headers
        },
      }
    );
    const listValue = response.data.orderObj.orders;
    return listValue;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      console.log(errorMessage);
      throw new Error(errorMessage); // Throw an error if login fails
    } else {
      console.error('An error occurred while getting orders', error);
      throw new Error('An error occurred while getting orders. Please try again.'); // Default error message
    }
  }
}

export async function NewOrder({ token, data }) {
  try {
    const response = await axios.post(`${API_BASE_URL}${endpoints.ORDER}`, data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const listValue = response.data.orderObj;

    return listValue;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      console.log(errorMessage);
      throw new Error(errorMessage); // Throw an error if login fails
    } else {
      console.error('An error occurred while creating order:', error);
      throw new Error('An error occurred while creating order. Please try again.'); // Default error message
    }
  }
}

export async function UpdateOrder({ token, id, formData }) {
  try {
    // Perform API call using Axios
    const response = await axios.put(`${API_BASE_URL}${endpoints.ORDER}/${id}`, formData, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const listValue = response.data.orderObj;
    return listValue;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      console.log(errorMessage);
      throw new Error(errorMessage);
    } else {
      console.error('An error occurred while updatng order:', error);
      throw new Error('An error occurred while updatng order. Please try again.');
    }
  }
}
