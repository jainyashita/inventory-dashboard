import axios from 'axios';

import API_BASE_URL, { endpoints } from '../../config/api';

// Define an async thunk for the login action
export async function InventoryList(token) {
  try {
    // Perform API call using Axios

    const response = await axios.get(`${API_BASE_URL}${endpoints.INVENTORYLIST}`, {
      headers: {
        Authorization: `Bearer ${token}`, // Include the authorization token in the request headers
      },
    });
    const listValue = response.data.inventoryObj;

    return listValue;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      throw new Error(errorMessage); // Throw an error if login fails
    } else {
      console.error('An error occurred while fetching inventory in:', error);
      throw new Error('An error occurred while fetching inventory. Please try again.'); // Default error message
    }
  }
}

export async function NewInventory({ token, data }) {
  try {
    // Perform API call using Axios

    const response = await axios.post(`${API_BASE_URL}${endpoints.INVENTORYLIST}`, data, {
      headers: {
        Authorization: `Bearer ${token}`, // Include the authorization token in the request headers
      },
    });
    const listValue = response.data.inventoryObj;

    return listValue;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      throw new Error(errorMessage); // Throw an error if login fails
    } else {
      console.error('An error occurred while creating inventory:', error);
      throw new Error('An error occurred while creating inventory. Please try again.');
    }
  }
}

export async function UpdateInventory({ token, id, formData }) {
  try {
    // Perform API call using Axios
    const response = await axios.put(
      `${API_BASE_URL}${endpoints.INVENTORYLIST}/${id}`,
      { name: formData.name, owner: formData.owner, inventoryManager: formData.inventoryManager },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    const listValue = response.data.inventoryObj;
    if (listValue) {
      return true;
    }
    return false;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      throw new Error(errorMessage); // Throw an error if login fails
    } else {
      console.error('An error occurred while updatng inventory:', error);
      throw new Error('An error occurred while updatng inventory. Please try again.'); // Default error message
    }
  }
}

export async function DeleteInventory({ token, id }) {
  try {
    // Perform API call using Axios

    const response = await axios.delete(`${API_BASE_URL}${endpoints.INVENTORYLIST}/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const listValue = response.data.inventoryObj;
    if (listValue) {
      return true;
    }
    return false;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      throw new Error(errorMessage); // Throw an error if login fails
    } else {
      console.error('An error occurred while deleting inventory:', error);
      throw new Error('An error occurred while deleting inventory. Please try again.'); // Default error message
    }
  }
}
