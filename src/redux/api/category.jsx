import axios from 'axios';

import API_BASE_URL, { endpoints } from '../../config/api';

export async function CategoryList(token, inventory) {
  try {
    console.log(inventory);
    console.log(`${API_BASE_URL}${endpoints.INVENTORYLIST}/${inventory}${endpoints.CATEGORYLIST}`);
    const response = await axios.get(
      `${API_BASE_URL}${endpoints.INVENTORYLIST}/${inventory}${endpoints.CATEGORYLIST}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    const listValue = response.data.inventoryObj;
    return listValue;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      throw new Error(errorMessage);
    } else {
      console.error('An error occurred while fetching category in:', error);
      throw new Error('An error occurred while fetching categories. Please try again.'); // Default error message
    }
  }
}

export async function CategoryListWithProducts(token, id) {
  try {
    console.log(id);
    const response = await axios.get(
      `${API_BASE_URL}${endpoints.INVENTORYLIST}/${id}${endpoints.CATEGORYPRODUCTCOUNT}`,
      {
        headers: {
          Authorization: `Bearer ${token}`, // Include the authorization token in the request headers
        },
      }
    );
    const listValue = response.data.productCount;
    console.log(listValue);
    return listValue;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      console.log(errorMessage);
      throw new Error(errorMessage);
    } else {
      console.error('An error occurred while fetching category in:', error);
      throw new Error('An error occurred while fetching categories. Please try again.'); // Default error message
    }
  }
}

export async function getCategory({ token, id }) {
  try {
    console.log(id);
    const response = await axios.get(`${API_BASE_URL}${endpoints.CATEGORYLIST}/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const listValue = response.data.categoryObj;
    return listValue;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      throw new Error(errorMessage);
    } else {
      console.error('An error occurred while fetching category in:', error);
      throw new Error('An error occurred while fetching categories. Please try again.'); // Default error message
    }
  }
}

export async function NewCategory({ token, data }) {
  try {
    // Perform API call using Axios
    const response = await axios.post(`${API_BASE_URL}${endpoints.CATEGORYLIST}`, data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const listValue = response.data.categoryObj;

    return listValue;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      console.log(errorMessage);
      throw new Error(errorMessage); // Throw an error if login fails
    } else {
      console.error('An error occurred while creating category:', error);
      throw new Error('An error occurred while creating category. Please try again.'); // Default error message
    }
  }
}

export async function UpdateCategory({ token, id, formData }) {
  try {
    // Perform API call using Axios
    const response = await axios.put(
      `${API_BASE_URL}${endpoints.CATEGORYLIST}/${id}`,
      {
        name: formData.name,
        owner: formData.owner,
        desc: formData.desc,
        gstSlab: formData.gstSlab,
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    const listValue = response.data.categoryObj;

    return listValue;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      console.log(errorMessage);
      throw new Error(errorMessage); // Throw an error if login fails
    } else {
      console.error('An error occurred while updating category:', error);
      throw new Error('An error occurred while updating category. Please try again.'); // Default error message
    }
  }
}

export async function DeleteCategory({ token, id }) {
  try {
    // Perform API call using Axios
    console.log(`value us this only ${API_BASE_URL}${endpoints.CATEGORYLIST}/${id}`);

    const response = await axios.delete(`${API_BASE_URL}${endpoints.CATEGORYLIST}/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const listValue = response.data.categoryObj;
    if (listValue) {
      return true;
    }
    return false;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      console.log(errorMessage);
      throw new Error(errorMessage); // Throw an error if login fails
    } else {
      console.error('An error occurred while deleting category:', error);
      throw new Error('An error occurred while deleting category. Please try again.'); // Default error message
    }
  }
}

export async function UploadCategoryImage({ token, formData, id }) {
  try {
    const response = await axios.post(
      `${API_BASE_URL}${endpoints.CATEGORYLIST}/${id}${endpoints.UPLOADIMAGE}`,
      formData,
      {
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'multipart/form-data',
        },
      }
    );
    const listValue = response.data.imageObj.category;

    return listValue;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      console.log(errorMessage);
      throw new Error(errorMessage); // Throw an error if login fails
    } else {
      console.error('An error occurred while uploading image:', error);
      throw new Error('An error occurred while uploading image. Please try again.'); // Default error message
    }
  }
}
