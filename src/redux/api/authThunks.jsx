// src/redux/actions/authThunks.js

import axios from 'axios';
import { createAsyncThunk } from '@reduxjs/toolkit';

import { history } from 'src/utils/history';

import { InventoryList } from './inventory';
import API_BASE_URL, { endpoints } from '../../config/api';
import { setInventoryList } from '../reducers/inventorySlice';
import { loginFailure, loginSuccess } from '../reducers/authSlice';

// Define an async thunk for the login action
export const login = createAsyncThunk('auth/login', async (credentials, { dispatch }) => {
  try {
    // Perform API call using Axios
    const response = await axios.post(`${API_BASE_URL}${endpoints.LOGIN}`, credentials);
    const { token, user } = response.data;
    let inventory;
    if (user && user.inventory) {
      ({ inventory } = user);
    } else if (user && user.userType === 'ADMIN') {
      const inventoryList = await InventoryList(token);
      dispatch(setInventoryList(inventoryList));

      if (inventoryList.length > 0) {
        inventory = inventoryList[0]._id;
      }
    }
    dispatch(loginSuccess({ token, user, inventory }));

    // get return url from location state or default to home page
    const { from } = history.location.state || { from: { pathname: '/' } };
    history.navigate(from);
    return { token, user }; // Return the token if successful
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      console.log(errorMessage);
      dispatch(loginFailure(errorMessage));
      throw new Error(errorMessage); // Throw an error if login fails
    } else {
      console.error('An error occurred while logging in:', error);
      throw new Error('An error occurred while logging in. Please try again.'); // Default error message
    }
  }
});

export const logout = createAsyncThunk('auth/logout', async (arg, { dispatch }) => {
  dispatch(logout(null));
  history.navigate('/login');
});
