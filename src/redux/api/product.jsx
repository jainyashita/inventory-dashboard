import axios from 'axios';

import API_BASE_URL, { endpoints } from '../../config/api';

export async function ProductList(id, token, page, limit) {
  try {
    // Perform API call using Axios
    const response = await axios.get(
      `${API_BASE_URL}${endpoints.CATEGORYLIST}/${id}${endpoints.PRODUCT}`,
      {
        headers: {
          Authorization: `Bearer ${token}`, // Include the authorization token in the request headers
        },
        params: {
          page,
          limit,
        },
      }
    );
    const listValue = response.data.categoryObj;
    return listValue;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      console.log(errorMessage);
      throw new Error(errorMessage); // Throw an error if login fails
    } else {
      console.error('An error occurred while getting products', error);
      throw new Error('An error occurred while getting products. Please try again.'); // Default error message
    }
  }
}

export async function fetchProductsBelowROP(id, token) {
  try {
    const response = await axios.get(
      `${API_BASE_URL}${endpoints.INVENTORYLIST}/${id}${endpoints.PRODUCTBELOWROP}`,
      {
        headers: {
          Authorization: `Bearer ${token}`, // Include the authorization token in the request headers
        },
      }
    );
    const listValue = response.data.productObj;
    return listValue;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      console.log(errorMessage);
      throw new Error(errorMessage); // Throw an error if login fails
    } else {
      console.error('An error occurred while getting products', error);
      throw new Error('An error occurred while getting products. Please try again.'); // Default error message
    }
  }
}

export async function InventoryStock(token, id) {
  try {
    // Perform API call using Axios

    const response = await axios.get(
      `${API_BASE_URL}${endpoints.INVENTORYLIST}/${id}${endpoints.STOCK}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    const listValue = response.data.inventoryStock;

    return listValue;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      console.log(errorMessage);
      throw new Error(errorMessage); // Throw an error if login fails
    } else {
      console.error('An error occurred while getting products count', error);
      throw new Error('An error occurred while getting products count. Please try again.'); // Default error message
    }
  }
}

export async function getProduct({ token, id }) {
  try {
    // Perform API call using Axios

    const response = await axios.get(`${API_BASE_URL}${endpoints.PRODUCT}/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const listValue = response.data.productObj;

    return listValue;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      console.log(errorMessage);
      throw new Error(errorMessage); // Throw an error if login fails
    } else {
      console.error('An error occurred while fetching product:', error);
      throw new Error('An error occurred while fetching product. Please try again.'); // Default error message
    }
  }
}

export async function NewProduct({ token, data }) {
  try {
    const response = await axios.post(`${API_BASE_URL}${endpoints.PRODUCT}`, data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const listValue = response.data.productObj;

    return listValue;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      console.log(errorMessage);
      throw new Error(errorMessage);
    } else {
      console.error('An error occurred while creating product:', error);
      throw new Error('An error occurred while creating product. Please try again.');
    }
  }
}

export async function UpdateProduct({ token, id, data }) {
  try {
    // Perform API call using Axios
    const response = await axios.put(`${API_BASE_URL}${endpoints.PRODUCT}/${id}`, data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const listValue = response.data.productObj;
    if (listValue) {
      return true;
    }
    return false;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      console.log(errorMessage);
      throw new Error(errorMessage); // Throw an error if login fails
    } else {
      console.error('An error occurred while updatng inventory:', error);
      throw new Error('An error occurred while updatng inventory. Please try again.'); // Default error message
    }
  }
}

export async function DeleteProduct({ token, id }) {
  try {
    // Perform API call using Axios

    const response = await axios.delete(`${API_BASE_URL}${endpoints.PRODUCT}/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const listValue = response.data.productObj;
    if (listValue) {
      return true;
    }
    return false;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      console.log(errorMessage);
      throw new Error(errorMessage); // Throw an error if login fails
    } else {
      console.error('An error occurred while deleting product:', error);
      throw new Error('An error occurred while deleting product. Please try again.'); // Default error message
    }
  }
}

export async function UploadProductImage({ token, formData, id }) {
  try {
    // Perform API call using Axios
    const response = await axios.post(
      `${API_BASE_URL}${endpoints.PRODUCT}/${id}${endpoints.UPLOADIMAGE}`,
      formData,
      {
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'multipart/form-data',
        },
      }
    );
    const listValue = response.data.productObj;

    return listValue;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      console.log(errorMessage);
      throw new Error(errorMessage); // Throw an error if login fails
    } else {
      console.error('An error occurred while creating inventory:', error);
      throw new Error('An error occurred while creating inventory. Please try again.'); // Default error message
    }
  }
}

export async function RemoveProductImage({ token, id, imageUrl }) {
  try {
    const response = await axios.delete(
      `${API_BASE_URL}${endpoints.PRODUCT}/${id}${endpoints.REMOVEIMAGE}`,

      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
        data: {
          imageUrl, // Correctly pass imageUrl in the request body
        },
      }
    );
    const listValue = response.data.imageObj;
    if (listValue) {
      return listValue;
    }
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      console.log(errorMessage);
      throw new Error(errorMessage); // Throw an error if login fails
    } else {
      console.error('An error occurred while removing image:', error);
      throw new Error('An error occurred while removing image. Please try again.'); // Default error message
    }
  }
}
