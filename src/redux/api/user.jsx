import axios from 'axios';

import API_BASE_URL, { endpoints } from 'src/config/api';

export async function UserList(token) {
  try {
    // Perform API call using Axios

    const response = await axios.get(`${API_BASE_URL}${endpoints.USER}`, {
      headers: {
        Authorization: `Bearer ${token}`, // Include the authorization token in the request headers
      },
    });
    const listValue = response.data.userObj;
    return listValue;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      console.log(errorMessage);
      throw new Error(errorMessage); // Throw an error if login fails
    } else {
      console.error('An error occurred while fetching users:', error);
      throw new Error('An error occurred while fetching users. Please try again.'); // Default error message
    }
  }
}

export async function InventoryWorkersList(token, inventory) {
  try {
    // Perform API call using Axios
    const inventoryId = inventory;
    console.log(`${API_BASE_URL}${endpoints.USER}/${inventoryId}${endpoints.WORKER}`);
    const response = await axios.get(
      `${API_BASE_URL}${endpoints.USER}/${inventoryId}${endpoints.WORKER}`,
      {
        headers: {
          Authorization: `Bearer ${token}`, // Include the authorization token in the request headers
        },
      }
    );
    const listValue = response.data.userObj;
    return listValue;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      console.log(errorMessage);
      throw new Error(errorMessage); // Throw an error if login fails
    } else {
      console.error('An error occurred while fetching workers:', error);
      throw new Error('An error occurred while fetching workers. Please try again.'); // Default error message
    }
  }
}

export async function UserCount(token) {
  try {
    // Perform API call using Axios

    const response = await axios.get(`${API_BASE_URL}${endpoints.USER}${endpoints.COUNT}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const listValue = response.data.userCount;

    return listValue;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      console.log(errorMessage);
      throw new Error(errorMessage); // Throw an error if login fails
    } else {
      console.error('An error occurred while getting users count', error);
      throw new Error('An error occurred while getting users count. Please try again.'); // Default error message
    }
  }
}

export async function NewUser({ token, data }) {
  try {
    // Perform API call using Axios
    console.log('data', data);
    const response = await axios.post(`${API_BASE_URL}${endpoints.REGISTER}`, data, {
      headers: {
        Authorization: `Bearer ${token}`, // Include the authorization token in the request headers
      },
    });
    const listValue = response.data;

    return listValue;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      console.log(errorMessage);
      throw new Error(errorMessage); // Throw an error if login fails
    } else {
      console.error('An error occurred while creating user:', error);
      throw new Error('An error occurred while creating user. Please try again.'); // Default error message
    }
  }
}

export async function UpdateUser({ token, id, formData }) {
  try {
    // Perform API call using Axios
    const response = await axios.put(`${API_BASE_URL}${endpoints.USER}/${id}`, formData, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    const listValue = response.data.userObj;

    return listValue;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      console.log(errorMessage);
      throw new Error(errorMessage); // Throw an error if login fails
    } else {
      console.error('An error occurred while updatng inventory:', error);
      throw new Error('An error occurred while updatng inventory. Please try again.'); // Default error message
    }
  }
}

export async function DeleteUser({ token, id, formData }) {
  try {
    // Perform API call using Axios
    const response = await axios.delete(`${API_BASE_URL}${endpoints.USER}/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const listValue = response.data.userObj;
    if (listValue) {
      return true;
    }
    return false;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      console.log(errorMessage);
      throw new Error(errorMessage); // Throw an error if login fails
    } else {
      console.error('An error occurred while deleting user:', error);
      throw new Error('An error occurred while deleting user. Please try again.'); // Default error message
    }
  }
}

export async function UpdateAddress({ token, id, formData }) {
  try {
    // Perform API call using Axios
    const response = await axios.put(`${API_BASE_URL}${endpoints.ADDRESS}/${id}`, formData, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    const listValue = response.data.addressObj;

    return listValue;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      console.log(errorMessage);
      throw new Error(errorMessage); // Throw an error if login fails
    } else {
      console.error('An error occurred while updating address:', error);
      throw new Error('An error occurred while updating address. Please try again.'); // Default error message
    }
  }
}

export async function NewAddress({ token, data }) {
  try {
    // Perform API call using Axios
    console.log('data', data);
    const response = await axios.post(`${API_BASE_URL}${endpoints.ADDRESS}`, data, {
      headers: {
        Authorization: `Bearer ${token}`, // Include the authorization token in the request headers
      },
    });
    const listValue = response.data.addressObj;

    return listValue;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      console.log(errorMessage);
      throw new Error(errorMessage); // Throw an error if login fails
    } else {
      console.error('An error occurred while creating address:', error);
      throw new Error('An error occurred while creating address. Please try again.'); // Default error message
    }
  }
}

export async function getAddress({ token, id }) {
  try {
    const response = await axios.get(`${API_BASE_URL}${endpoints.ADDRESS}/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const listValue = response.data.addressObj;
    return listValue;
  } catch (error) {
    if (
      error.response &&
      error.response.status === 401 &&
      error.response.data &&
      error.response.data.errors
    ) {
      const errorMessage = error.response.data.errors;
      console.log(errorMessage);
      throw new Error(errorMessage);
    } else {
      console.error('An error occurred while getting address in:', error);
      throw new Error('An error occurred while getting address. Please try again.'); // Default error message
    }
  }
}
