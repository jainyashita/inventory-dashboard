export const addressSchema = [
  {
    name: 'addressLine1',
    label: 'Address Line 1',
    type: 'text',
    required: 'true',
  },
  {
    name: 'addressLine2',
    label: 'Address Line 2',
    type: 'text',
    required: 'true',
  },
  {
    name: 'landmark',
    label: 'Landmark',
    type: 'text',
    required: 'true',
  },
  {
    name: 'city',
    label: 'City',
    type: 'text',
    required: 'true',
  },
  {
    name: 'country',
    label: 'Country',
    type: 'text',
    required: 'true',
  },
  {
    name: 'postalCode',
    label: 'Postal Code',
    type: 'text',
    required: 'true',
  },
];

export const inventorySchema = [
  { name: 'name', label: 'Name', type: 'text', required: 'true' },
  { name: 'owner', label: 'Owner', type: 'dropdown', options: [], required: 'true' },
  {
    name: 'addressLine1',
    label: 'Address Line 1',
    type: 'text',
    required: 'true',
  },
  {
    name: 'addressLine2',
    label: 'Address Line 2',
    type: 'text',
    required: 'true',
  },
  {
    name: 'landmark',
    label: 'Landmark',
    type: 'text',
    required: 'true',
  },
  {
    name: 'city',
    label: 'City',
    type: 'text',
    required: 'true',
  },
  {
    name: 'country',
    label: 'Country',
    type: 'text',
    required: 'true',
  },
  {
    name: 'postalCode',
    label: 'Postal Code',
    type: 'text',
    required: 'true',
  },
];

export const userSchema = [
  { name: 'username', label: 'User Name', type: 'text', required: 'true' },
  {
    name: 'FirstName',
    label: 'First Name',
    type: 'text',
    required: 'true',
  },
  {
    name: 'LastName',
    label: 'Last Name',
    type: 'text',
    required: 'true',
  },
  {
    name: 'userType',
    label: 'User Type',
    type: 'dropdown',
    options: [],
    required: true,
  },
  {
    name: 'inventory',
    label: 'Inventory',
    type: 'dropdown',
    options: [],
  },
  {
    name: 'password',
    label: 'Password',
    type: 'text',
  },
  {
    name: 'mobile',
    label: 'Contact',
    type: 'text',
    required: 'true',
  },
  { name: 'email', label: 'E-Mail', type: 'text', required: 'true' },
  {
    name: 'addressLine1',
    label: 'Address Line 1',
    type: 'text',
    required: 'true',
  },
  {
    name: 'addressLine2',
    label: 'Address Line 2',
    type: 'text',
    required: 'true',
  },
  {
    name: 'landmark',
    label: 'Landmark',
    type: 'text',
    required: 'true',
  },
  {
    name: 'city',
    label: 'City',
    type: 'text',
    required: 'true',
  },
  {
    name: 'country',
    label: 'Country',
    type: 'text',
    required: 'true',
  },
  {
    name: 'postalCode',
    label: 'Postal Code',
    type: 'text',
    required: 'true',
  },
];

export const categorySchema = [
  {
    name: 'name',
    label: 'Name',
    required: 'true',
    type: 'text',
  },
  {
    name: 'desc',
    label: 'Description',
    required: 'true',
    type: 'text',
  },

  {
    name: 'gstSlab',
    label: 'GST Slab Id',
    required: 'true',
    type: 'dropdown',
    options: [], // Placeholder for options, will be fetched from API
  },
];

export const gstSlabSchema = [
  {
    name: 'name',
    label: 'Name',
    type: 'text',
    required: 'true',
  },
  {
    name: 'rate',
    label: 'Rate',
    type: 'text',
    required: 'true',
  },
];

export const productSchema = [
  {
    name: 'name',
    label: 'Name',
    type: 'text',
    required: 'true',
  },
  {
    name: 'desc',
    label: 'Description',
    type: 'text',
    required: 'true',
  },
  {
    name: 'SKU',
    label: 'SKU',
    type: 'text',
    required: 'true',
  },
  {
    name: 'ROP',
    label: 'ROP',
    type: 'text',
  },
  {
    name: 'price',
    label: 'Price',
    type: 'text',
    required: 'true',
  },

  {
    name: 'quantity',
    label: 'Quantity',
    type: 'text',
    required: 'true',
  },
  {
    name: 'specifications',
    label: 'Specifications',
    type: 'object',
    required: true,
  },
  {
    name: 'rating',
    label: 'Rating out of 5',
    type: 'text',
    required: 'true',
  },
];
