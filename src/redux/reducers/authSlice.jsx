// src/redux/reducers/authSlice.js

import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  user: null,
  token: null,
  inventory: null,
  error: null,
};

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    loginSuccess(state, action) {
      state.user = action.payload.user;
      state.token = action.payload.token;
      state.inventory = action.payload.inventory;
      state.error = null;
    },
    updateInventory: (state, action) => {
      state.inventory = action.payload.inventory;
    },
    loginFailure(state, action) {
      state.user = null;
      state.token = null;
      state.inventory = null;
      state.error = action.payload;
    },
    logout(state) {
      state.user = null;
      state.token = null;
      state.inventory = null;
      state.error = null;
    },
  },
});

export const { loginSuccess, loginFailure, logout, updateInventory } = authSlice.actions;
export default authSlice.reducer;
