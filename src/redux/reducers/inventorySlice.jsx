import { createSlice } from '@reduxjs/toolkit';

const inventorySlice = createSlice({
  name: 'inventory',
  initialState: {
    list: [],
    selected: null,
  },
  reducers: {
    setInventoryList: (state, action) => {
      state.list = action.payload;
    },
    setSelectedInventory: (state, action) => {
      state.selected = action.payload;
    },
  },
});

export const { setInventoryList, setSelectedInventory } = inventorySlice.actions;
export default inventorySlice.reducer;
